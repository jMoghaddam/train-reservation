#include "raylib.h"
#include <stdio.h> 
#include "check_login.h"
#include "signup.h"

#define myDarkBlue CLITERAL{36, 48, 94, 255}
#define myLightBlue CLITERAL{55, 71, 133, 255}
#define mySkyBlue CLITERAL{168, 208, 230, 255}
#define myRed CLITERAL{247, 108, 108, 255}
#define myYellow CLITERAL{248, 233, 161, 255}
#define myTransparent CLITERAL{0, 0, 0, 0}

/*
    Type Implementation
*/
void login_input(int key, char text[19], int *letterCounter, int max) 
{
    if ((key >= 32) && (key <= 125) && (*letterCounter < max))
    {
        text[*letterCounter] = (char)key;
        (*letterCounter)++;
    }

    if (IsKeyPressed(KEY_BACKSPACE))
    {
        (*letterCounter)--;
        if (*letterCounter < 0) *letterCounter = 0;
        text[*letterCounter] = '\0';
    }
}

/********************************************************************

    Login Screen

********************************************************************/
void run_login(const int screenWidth, const int screenHeight) 
{
    /* Initializing Window */
    InitWindow(screenWidth, screenHeight, "Train Reservation");

    /* Fonts */
    Font montserrat_regular = LoadFont("resources/fonts/montserrat.ttf");
    Font montserrat_small = LoadFontEx("resources/fonts/montserrat.ttf", 24, 0, 250);
    Font montserrat_big = LoadFontEx("resources/fonts/montserrat.ttf", 48, 0, 250);


    /* Rectangles */
    Rectangle container = { screenWidth / 2 - 200, screenHeight / 2 - 250, 400, 500 };
    Rectangle uname_box = { screenWidth / 2 - 160, screenHeight / 2 - 84, 320, 50 };
    Rectangle pass_box = { screenWidth / 2 - 160, screenHeight / 2 + 46, 320, 50 };
    Rectangle submit_box = { screenWidth / 2 - 160, screenHeight / 2 + 146, 160, 50 };
    Rectangle signup_box = { screenWidth / 2 + 5 , screenHeight / 2 + 135, 170, 70 };


    /* Is the Box Clicked? */
    bool IC_uname_input = false;
    bool IC_pass_input = false;


    /* Varying Strings */
    char error_msg[100] = " ";


    /* String Holders and Letter Counters for Input Box */
    char username[18 + 1] = "\0";
    char password[18 + 1] = "\0";
    char passholder[18 + 1] = "\0";
    int  uname_letterCount = 0;
    int  pass_letterCount = 0;

    char userid[10];


    int temp = 0;     /* Is Loginer Admin or User */
    int con = 0;      /* Break the while When a Button is Clicked */
    SetTargetFPS(60);
    while (!WindowShouldClose() && con == 0)
    {

        /* Check if Input Box was CLicked */
        if (CheckCollisionPointRec(GetMousePosition(), uname_box) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) IC_uname_input = true;
        else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) IC_uname_input = false;

        /* Check if Input Box was CLicked */
        if (CheckCollisionPointRec(GetMousePosition(), pass_box) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) IC_pass_input = true;
        else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) IC_pass_input = false;

        /* If Input Box was Clicked: */
        if (IC_uname_input)
        {
            int key = GetKeyPressed();
            login_input(key, username, &uname_letterCount, 18);
        }

        /* If Input Box was Clicked: */
        if (IC_pass_input)
        {
            int key = GetKeyPressed();
            if ((key >= 32) && (key <= 125) && (pass_letterCount < 18))
            {
                password[pass_letterCount] = (char)key;
                passholder[pass_letterCount] = '*';
                pass_letterCount++;
            }

            if (IsKeyPressed(KEY_BACKSPACE))
            {
                pass_letterCount--;
                if (pass_letterCount < 0) pass_letterCount = 0;
                password[pass_letterCount] = '\0';
                passholder[pass_letterCount] = '\0';
            }
        }

        /* UI Design */
        BeginDrawing();

        ClearBackground(mySkyBlue);

        DrawRectangleRec(container, myDarkBlue);
        DrawRectangleRec(uname_box, WHITE);
        DrawRectangleRec(pass_box, WHITE);
        DrawRectangleRec(submit_box, myRed);
        DrawRectangleRec(signup_box, myTransparent);

        DrawTextEx(montserrat_regular, "Login to your account", (Vector2) { screenWidth / 2 - 160, screenHeight / 2 - 230 }, montserrat_regular.baseSize, 2, WHITE);
        DrawTextEx(montserrat_regular, "Username", (Vector2) { screenWidth / 2 - 160, screenHeight / 2 - 130 }, montserrat_regular.baseSize, 2, WHITE);
        DrawTextEx(montserrat_regular, "Password", (Vector2) { screenWidth / 2 - 160, screenHeight / 2 }, montserrat_regular.baseSize, 2, WHITE);
        DrawTextEx(montserrat_small, "No account?", (Vector2) { screenWidth / 2 + 20, screenHeight / 2 + 161 }, montserrat_small.baseSize, 2, WHITE);
        DrawTextEx(montserrat_regular, "Login", (Vector2) { screenWidth / 2 - 121, screenHeight / 2 + 155 }, montserrat_regular.baseSize, 2, WHITE);
        DrawTextEx(montserrat_regular, error_msg, (Vector2) { container.x ,container.y + 510  }, montserrat_regular.baseSize, 2, RED);
        DrawTextEx(montserrat_regular, username, (Vector2) { uname_box.x + 10, uname_box.y + 8 }, montserrat_regular.baseSize, 2, BLACK);
        DrawTextEx(montserrat_big, passholder, (Vector2) { pass_box.x + 5, pass_box.y + 8 }, montserrat_big.baseSize, 2, BLACK);

        if (IC_uname_input) DrawRectangleLinesEx(uname_box, 2, myRed);

        if (IC_pass_input) DrawRectangleLinesEx(pass_box, 2, myRed);

        EndDrawing();

        /* If Signup was Clicked: */
        if (CheckCollisionPointRec(GetMousePosition(), signup_box) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) 
        {
            con = -1;
        }

        /* If Submit was Clicked: */
        if (CheckCollisionPointRec(GetMousePosition(), submit_box) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) 
        {
            /* If Boxes were not Empty: */
            if ( username[0] != '\0' && password[0] != '\0') 
            {
                /* If Successfully Connected to Database */
                if (connect()) 
                {
                    temp = select(username, password, userid);
                    if (temp == 0) {
                        strcpy(error_msg, "Wrong username or password");
                        close();
                    }
                    else if (temp == -1) 
                    {
                        strcpy(error_msg, "Error connecting to database");
                        close();
                    }
                    else if (temp == 1) 
                    {
                        con = 1;
                        close();
                    }
                    else if (temp == 2) 
                    {
                        con = -1;
                        close();
                    }
                }
                else 
                {
                    strcpy(error_msg, "Error connecting to database");
                }
            }
            else 
            {
                strcpy(error_msg, "Box is empty");
            }
        }

    }

    UnloadFont(montserrat_regular);
    UnloadFont(montserrat_small);
    UnloadFont(montserrat_big);

    CloseWindow();

    if (temp == 1) {
        run_admin_panel(screenWidth, screenHeight);
    }
    else if (temp == 2) {
        int temp = TextToInteger(userid);
        run_user_panel(screenWidth, screenHeight, temp);
    }
    else if (con == -1) {
        run_signup(screenWidth, screenHeight);
    }
}