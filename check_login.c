#include <stdio.h>
#include <sqlite3.h> 
#include <string.h>
#pragma warning(disable: 4996)

sqlite3* db;
char pass[30];
char tempid[10];

int callback(void* NotUsed, int argc, char** argv, char** azColName) {

    strcpy(pass, argv[0]);

    return 0;
}

int ucallback(void* NotUsed, int argc, char** argv, char** azColName) {

    strcpy(pass, argv[4]);
    strcpy(tempid, argv[0]);

    return 0;
}

int connect() {

    int rc;
    rc = sqlite3_open("Resources/databases/database.db", &db);

    if (rc) {
        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
        return 0;
    }
    
    return 1;

}

int select(char uname[], char pword[], char userid[]) {

    int rc;
    char sql[1000];
    sprintf(sql, "SELECT password FROM admins WHERE username = \'%s\'", uname);
    char* zErrMsg = 0;
    rc = sqlite3_exec(db, sql , callback, 0, &zErrMsg);

    if (rc != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        return -1;
    }

    if (strcmp(pass, pword) == 0) {
        return 1;
    }
   
    int urc;
    char usql[1000];
    sprintf(usql, "SELECT * FROM users WHERE username = \'%s\'", uname);
    char* uzErrMsg = 0;
    urc = sqlite3_exec(db, usql, ucallback, 0, &uzErrMsg);

    if (urc != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", uzErrMsg);
        sqlite3_free(uzErrMsg);
        return -1;
    }

    strcpy(userid, tempid);

    if (strcmp(pass, pword) == 0) {
        return 2;
    }

    return 0;
    
}

int close() {
    sqlite3_close(db);
}