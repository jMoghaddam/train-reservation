#include "raylib.h"
#include <string.h>
#include <stdio.h>
#include "login.h"
#include "check_login.h"
#include "signup_check.h"
#include "admin_panel.h"
#pragma warning (disable: 4996)

#define myDarkBlue CLITERAL{36, 48, 94, 255}
#define myLightBlue CLITERAL{55, 71, 133, 255}
#define mySkyBlue CLITERAL{168, 208, 230, 255}
#define myRed CLITERAL{247, 108, 108, 255}
#define myYellow CLITERAL{248, 233, 161, 255}
#define myTransparent CLITERAL{0, 0, 0, 0}

int uid;

int run_signup(const int screenWidth, const int screenHeight)
{
    InitWindow(screenWidth, screenHeight, "Train Reservation");

    Image back = LoadImage("Resources/mockup/images/back.png");
    ImageResize(&back, 80, 80);
    Texture2D texture = LoadTextureFromImage(back);
    UnloadImage(back);

    Font montserrat_regular = LoadFontEx("resources/fonts/montserrat.ttf", 40, 0, 250);

    char signup_msg[] = "Create a new account";
    char uname_msg[] = "Username";
    char fname_msg[] = "First Name";
    char lname_msg[] = "Last Name";
    char pass_msg[] = "Password";
    char submit_msg[] = "Signup";
    char error_msg[100] = " ";

    char username[18 + 1] = "\0";
    char firstname[18 + 1] = "\0";
    char lastname[18 + 1] = "\0";
    char password[18 + 1] = "\0";

    int  uname_letterCount = 0;
    int  fname_letterCount = 0;
    int  lname_letterCount = 0;
    int  pass_letterCount = 0;

    bool uname_mouseOnText = false;
    bool fname_mouseOnText = false;
    bool lname_mouseOnText = false;
    bool pass_mouseOnText = false;

    Rectangle container = { screenWidth / 2 - 250, screenHeight / 2 - 375, 500, 800 };
    Rectangle uname_box = { screenWidth / 2 - 200, screenHeight / 2 - 200, 400, 60 };
    Rectangle fname_box = { screenWidth / 2 - 200, screenHeight / 2 - 50, 400, 60 };
    Rectangle lname_box = { screenWidth / 2 - 200, screenHeight / 2 + 100, 400, 60 };
    Rectangle pass_box = { screenWidth / 2 - 200, screenHeight / 2 + 250, 400, 60 };
    Rectangle submit_box = { screenWidth / 2 - 90, screenHeight / 2 + 340, 180, 60 };
    Rectangle back_box = {  30, 50 , 100, 60 };

    int con = 1;
    int exist_temp = 0;
    int create_temp = 0;
    SetTargetFPS(60);
    while (!WindowShouldClose() && con == 1) {

        if (CheckCollisionPointRec(GetMousePosition(), uname_box) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) uname_mouseOnText = true;
        else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) uname_mouseOnText = false;

        if (CheckCollisionPointRec(GetMousePosition(), fname_box) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) fname_mouseOnText = true;
        else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) fname_mouseOnText = false;

        if (CheckCollisionPointRec(GetMousePosition(), lname_box) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) lname_mouseOnText = true;
        else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) lname_mouseOnText = false;

        if (CheckCollisionPointRec(GetMousePosition(), pass_box) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) pass_mouseOnText = true;
        else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) pass_mouseOnText = false;

        if (uname_mouseOnText)
        {
            int key = GetKeyPressed();

            if ((key >= 32) && (key <= 125) && (uname_letterCount < 18))
            {
                username[uname_letterCount] = (char)key;
                uname_letterCount++;
            }

            if (IsKeyPressed(KEY_BACKSPACE))
            {
                uname_letterCount--;
                if (uname_letterCount < 0) uname_letterCount = 0;
                username[uname_letterCount] = '\0';
            }
        }

        if (fname_mouseOnText)
        {
            int key = GetKeyPressed();

            if ((key >= 32) && (key <= 125) && (fname_letterCount < 18))
            {
                firstname[fname_letterCount] = (char)key;
                fname_letterCount++;
            }

            if (IsKeyPressed(KEY_BACKSPACE))
            {
                fname_letterCount--;
                if (fname_letterCount < 0) fname_letterCount = 0;
                firstname[fname_letterCount] = '\0';
            }
        }

        if (lname_mouseOnText)
        {
            int key = GetKeyPressed();

            if ((key >= 32) && (key <= 125) && (lname_letterCount < 18))
            {
                lastname[lname_letterCount] = (char)key;
                lname_letterCount++;
            }

            if (IsKeyPressed(KEY_BACKSPACE))
            {
                lname_letterCount--;
                if (lname_letterCount < 0) lname_letterCount = 0;
                lastname[lname_letterCount] = '\0';
            }
        }

        if (pass_mouseOnText)
        {
            int key = GetKeyPressed();

            if ((key >= 32) && (key <= 125) && (pass_letterCount < 18))
            {
                password[pass_letterCount] = (char)key;
                pass_letterCount++;
            }

            if (IsKeyPressed(KEY_BACKSPACE))
            {
                pass_letterCount--;
                if (pass_letterCount < 0) pass_letterCount = 0;
                password[pass_letterCount] = '\0';
            }
        }

        BeginDrawing();

        ClearBackground(myDarkBlue);

        DrawTexture(texture, texture.width / 2, texture.height / 2, WHITE);

        DrawRectangleRec(container, WHITE);
        DrawRectangleLinesEx(uname_box, 3, myDarkBlue);
        DrawRectangleLinesEx(fname_box, 3, myDarkBlue);
        DrawRectangleLinesEx(lname_box, 3, myDarkBlue);
        DrawRectangleLinesEx(pass_box, 3, myDarkBlue);
        DrawRectangleRec(submit_box, myRed);
        DrawRectangleRec(submit_box, myTransparent);

        if (uname_mouseOnText) DrawRectangleLinesEx(uname_box, 4, myDarkBlue);
        if (fname_mouseOnText) DrawRectangleLinesEx(fname_box, 4, myDarkBlue);
        if (lname_mouseOnText) DrawRectangleLinesEx(lname_box, 4, myDarkBlue);
        if (pass_mouseOnText) DrawRectangleLinesEx(pass_box, 4, myDarkBlue);

        DrawTextEx(montserrat_regular, signup_msg, (Vector2) { screenWidth / 2 - 200, screenHeight / 2 - 350 }, montserrat_regular.baseSize, 2, myDarkBlue);
        DrawTextEx(montserrat_regular, uname_msg, (Vector2) { screenWidth / 2 - 200, screenHeight / 2 - 250 }, montserrat_regular.baseSize, 2, myRed);
        DrawTextEx(montserrat_regular, fname_msg, (Vector2) { screenWidth / 2 - 200, screenHeight / 2 - 100 }, montserrat_regular.baseSize, 2, myRed);
        DrawTextEx(montserrat_regular, lname_msg, (Vector2) { screenWidth / 2 - 200, screenHeight / 2 + 50 }, montserrat_regular.baseSize, 2, myRed);
        DrawTextEx(montserrat_regular, pass_msg, (Vector2) { screenWidth / 2 - 200, screenHeight / 2 + 200 }, montserrat_regular.baseSize, 2, myRed);
        DrawTextEx(montserrat_regular, submit_msg, (Vector2) { screenWidth / 2 - 60, screenHeight / 2 + 350 }, montserrat_regular.baseSize, 2, WHITE);
        DrawTextEx(montserrat_regular, error_msg, (Vector2) { container.x, container.y + 810 }, montserrat_regular.baseSize, 2, RED);

        DrawTextEx(montserrat_regular, username, (Vector2) { uname_box.x + 10, uname_box.y + 8 }, montserrat_regular.baseSize, 2, BLACK);
        DrawTextEx(montserrat_regular, firstname, (Vector2) { fname_box.x + 10, fname_box.y + 8 }, montserrat_regular.baseSize, 2, BLACK);
        DrawTextEx(montserrat_regular, lastname, (Vector2) { lname_box.x + 10, lname_box.y + 8 }, montserrat_regular.baseSize, 2, BLACK);
        DrawTextEx(montserrat_regular, password, (Vector2) { pass_box.x + 10, pass_box.y + 8 }, montserrat_regular.baseSize, 2, BLACK);

        EndDrawing();

        if (CheckCollisionPointRec(GetMousePosition(), back_box) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
            con = 0;
        }

        if (CheckCollisionPointRec(GetMousePosition(), submit_box) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
            if (username[0] != '\0' && firstname[0] != '\0' && lastname[0] != '\0') {
                if (pass_letterCount > 4) {
                    if (sc_connect()) {
                        exist_temp = exist(username);
                        if (exist_temp == -1) {
                            strcpy(error_msg, "Error connecting to database");
                            sc_close();
                        }
                        else if (exist_temp == 0) {
                            strcpy(error_msg, "Username already exist");
                            printf("hi\n");
                            sc_close();
                        }
                        else if (exist_temp == 1){
                            create_temp = create(username, firstname, lastname, password);
                            if (create_temp == -1) {
                                strcpy(error_msg, "Error connecting to database");
                                sc_close();
                            }
                            else if (create_temp == 1) {
                                con = -1;
                                sc_close();
                            }
                        }
                    }
                    else {
                        strcpy(error_msg, "Error connecting to database");
                    }
                }
                else {
                    strcpy(error_msg, "Password too short");
                }
            }
            else {
                strcpy(error_msg, "Box is empty");
            }
        }

    }

    UnloadTexture(texture);

    UnloadFont(montserrat_regular);

    CloseWindow();

    if (con == 0) {
        run_login(screenWidth, screenHeight);
    }
    else if (con == -1) {
        run_login(screenWidth, screenHeight);
    }
    
}