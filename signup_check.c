#include <stdio.h>
#include <sqlite3.h> 
#include <string.h>
#pragma warning(disable: 4996)

sqlite3* db;

int user_exist;
char temp[10];

int exist_callback(void* NotUsed, int argc, char** argv, char** azColName) {

    user_exist = 1;

    return 0;
}

int aexist_callback(void* NotUsed, int argc, char** argv, char** azColName) {

    user_exist = 1;

    return 0;
}

int create_callback(void* NotUsed, int argc, char** argv, char** azColName) {
    return 0;
}

int login_callback(void* NotUsed, int argc, char** argv, char** azColName) {

    strcpy(temp, argv[0]);

    return 0;
}

int sc_connect() {
    int rc;
    rc = sqlite3_open("Resources/databases/database.db", &db);

    if (rc) {
        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
        return 0;
    }

    return 1;
}

int exist(char uname[]) {

    user_exist = 0;

    int rc;
    char sql[1000];
    sprintf(sql, "SELECT id FROM users WHERE username = \'%s\'", uname);
    char* zErrMsg = 0;

    rc = sqlite3_exec(db, sql, exist_callback, 0, &zErrMsg);

    if (rc != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        return -1;
    }

    int arc;
    char asql[1000] = "SELECT id FROM admins WHERE username = \'";
    sprintf(asql, "SELECT id FROM admins WHERE username = \'%s\'", uname);
    char* azErrMsg = 0;

    arc = sqlite3_exec(db, asql, aexist_callback, 0, &azErrMsg);

    if (arc != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        return -1;
    }

    if (user_exist == 1) {
        return 0;
    }

    return 1;
}

int create(char uname[], char fname[], char lname[], char pword[]) {

    int rc;
    char sql[1000];
    sprintf(sql, "INSERT INTO users (firstname, lastname, username, password, wallet) VALUES (\'%s\' , \'%s\' , \'%s\' , \'%s\', 200000)", fname, lname, uname, pword);
    char* zErrMsg = 0;

    rc = sqlite3_exec(db, sql, create_callback, 0, &zErrMsg);

    if (rc != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        return -1;
    }

    return 1;

}

int sc_close() {
    sqlite3_close(db);
}