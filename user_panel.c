#include <stdio.h>
#include <string.h>
#include "raylib.h"
#include "sqlite3.h"
#include "check_login.h"

#pragma warning (disable: 4996)

#define myDarkBlue CLITERAL{36, 48, 94, 255}
#define myLightBlue CLITERAL{55, 71, 133, 255}
#define mySkyBlue CLITERAL{168, 208, 230, 255}
#define myRed CLITERAL{247, 108, 108, 255}
#define myTransparent CLITERAL{0, 0, 0, 0}

sqlite3* db;

int ids[100];
char companies[100][100];
char names[100][100];
char times[100][100];

int adultfee[100];
int childfee[100];
int comps[100];
int capacity[100];
int per[100];

int atrain_counter = 0;
int is_discount_valid = -1;

char user_fname[100];
char user_lname[100];
int money = -1;

void sort(int n, int* ptr)
{
    int i, j, t;

    // Sort the numbers using pointers 
    for (i = 0; i < n; i++) {

        for (j = i + 1; j < n; j++) {

            if (*(ptr + j) < *(ptr + i)) {

                t = *(ptr + i);
                *(ptr + i) = *(ptr + j);
                *(ptr + j) = t;
            }
        }
    }
}

int info_callback(void* NotUsed, int argc, char** argv, char** azColName) {

    strcpy(user_fname, argv[1]);
    strcpy(user_lname, argv[2]);
    money = TextToInteger(argv[5]);
    printf("heo");

    return 0;
}

int oneway_callback(void* NotUsed, int argc, char** argv, char** azColName) {

    ids[atrain_counter] = TextToInteger(argv[0]);

    strcpy(companies[atrain_counter], argv[1]);

    strcpy(names[atrain_counter], argv[2]);
    
    strcpy(times[atrain_counter], argv[6]);

    adultfee[atrain_counter] = TextToInteger(argv[9]);
    childfee[atrain_counter] = TextToInteger(argv[10]);

    comps[atrain_counter] = TextToInteger(argv[11]);
    capacity[atrain_counter] = TextToInteger(argv[12]);
    per[atrain_counter] = TextToInteger(argv[13]);

    atrain_counter++;

    return 0;
}

int dis_callback(void* NotUsed, int argc, char** argv, char** azColName) {

    is_discount_valid = TextToInteger(argv[0]);

    return 0;
}

int wallet_callback(void* NotUsed, int argc, char** argv, char** azColName) {

    money = TextToInteger(argv[0]);

    return 0;
}

int update_callback(void* NotUsed, int argc, char** argv, char** azColName) {

    return 0;
}

void get_info(int id)
{
    connect();

    int rc;
    char sql[1000];
    sprintf(sql, "SELECT * FROM users WHERE id = \'%d\'", id);
    char* zErrMsg = 0;
    rc = sqlite3_exec(db, sql, info_callback, 0, &zErrMsg);
}

void search_oneway(char from[100], char to[100], char raft_year[100], char raft_month[100], char raft_day[100], int adult_counter, int children_counter) 
{
    atrain_counter = 0;

    char date[100];
    sprintf(date, "%s/%s/%s", raft_year, raft_month, raft_day);
    int rc;
    char sql[1000];
    sprintf(sql, "SELECT * FROM trains WHERE origin = \'%s\' AND destination = \'%s\' AND date = \'%s\'", from, to, date);
    char* zErrMsg = 0;
    rc = sqlite3_exec(db, sql, oneway_callback, 0, &zErrMsg);
}

int add_discount(char x[100])
{
    char date[100];
    int rc;
    char sql[1000];
    sprintf(sql, "SELECT discount FROM discounts WHERE code = \'%s\'", x);
    char* zErrMsg = 0;
    rc = sqlite3_exec(db, sql, dis_callback, 0, &zErrMsg);

    return is_discount_valid;
}

int fs = 0;
int reservedsits[1000];
int findsits_callback(void* NotUsed, int argc, char** argv, char** azColName) {

    char temp[10];
    int i = 0;
    while (argv[0][i] != '\0')
    {
        int j = i;
        int k = 0;
        while (argv[0][j] != ' ' && argv[0][j] != '\0')
        {
            temp[k] = argv[0][j];

            j++;
            k++;
        }
        temp[k] = '\0';
        
        reservedsits[fs] = TextToInteger(temp);
        fs++;
        i = j + 1;
    }

    return 0;
}

int lastComp = 0;
int fc = 0;
int reservedcomps[1000];
int findcomp_callback(void* NotUsed, int argc, char** argv, char** azColName) {

    char temp[10];
    int i = 0;
    while (argv[0][i] != '\0')
    {
        int j = i;
        int k = 0;
        while (argv[0][j] != ' ' && argv[0][j] != '\0')
        {
            temp[k] = argv[0][j];

            j++;
            k++;
        }
        temp[k] = '\0';

        reservedcomps[fc] = TextToInteger(temp);
        fs++;
        i = j + 1;
    }

    lastComp = reservedcomps[fc];

    fc++;

    return 0;
}

int sitCounter = 0;
int compCounter = 0;
int sits[100];
int comps[100];
char strSits[100];
char strComps[100];
int purchase_ticket(int i, int uid, int id, int adult, int child, int price, bool full)
{
    if (money < price)
        return -1;

    money -= price;

    int mrc;
    char msql[1000];
    sprintf(msql, "UPDATE users SET wallet = %d WHERE id is %d", money, uid);
    char* mzErrMsg = 0;
    mrc = sqlite3_exec(db, msql, findcomp_callback, 0, &mzErrMsg);

    fs = 0;
    fc = 0;

    if (!full)
    {
        int rc;
        char sql[1000];
        sprintf(sql, "SELECT sits FROM reserved WHERE trainID = %d", id);
        char* zErrMsg = 0;
        rc = sqlite3_exec(db, sql, findsits_callback, 0, &zErrMsg);

        sort(fs, reservedsits);

        if (fs == 0)
        {
            int j = ((adult + child) / per[i]) + 1;
            for (int i = 0; i < j; i++)
            {
                comps[i] = i + 1;
                compCounter++;
            }

            for (int i = 0; i < (adult + child); i++)
            {
                sits[i] = i + 1;
                sitCounter++;
            }
        }
        else 
        {
            int s = 0;
            for (int i = 0; i < fs - 1; i++)
            {
                if (reservedsits[i + 1] - reservedsits[i] > (adult + child))
                {
                    for (int j = 0; j < (adult + child); j++)
                    {
                        sits[j] = reservedsits[i] + j + 1;
                        s = 1;
                        sitCounter++;
                    }
                    break;
                }
            }

            if (s == 0)
            {
                for (int k = 0; k < (adult + child); k++)
                {
                    sits[k] = reservedsits[fs - 1] + k + 1;
                    sitCounter++;
                }
            }

            int v = 0;
            for (int k = 0; k < (adult + child); k++)
            {
                if ( sits[k] % per[i] == 0 && comps[v - 1] != sits[k] / per[i] )
                {
                    comps[v] = sits[k] / per[i];
                    v++;
                    compCounter++;
                }
                else if (comps[v - 1] != sits[k] / per[i])
                {
                    comps[v] = sits[k] / per[i] + 1;
                    v++;
                    compCounter++;
                }
            }
        }
    }
    else
    {
        int rc;
        char sql[1000];
        sprintf(sql, "SELECT comp FROM reserved WHERE trainID = %d", id);
        char* zErrMsg = 0;
        rc = sqlite3_exec(db, sql, findcomp_callback, 0, &zErrMsg);

        sort(fc, reservedcomps);

        for (int w = 0; w < (((adult + child) / per[i]) + 1); w++)
        {
            comps[w] = lastComp + w + 1;
            compCounter++;
        }

        int v = 0;
        for (int k = 0; k < compCounter; k++)
        {
            for (int j = 0; j < 4; j++)
            {
                sits[v] = ( (comps[k] - 1) * per[i] ) + j + 1;
                v++;
                sitCounter++;
            }
        }
    }

    char firstSit[10];
    sprintf(firstSit, "%d", sits[0]);
    strcpy(strSits, firstSit);

    for (int i = 1; i < sitCounter; i++)
    {
        char temp[10];
        sprintf(temp, " %d", sits[i]);
        strcat(strSits, temp);
    }

    char firstComp[10];
    sprintf(firstComp, "%d", comps[0]);
    strcpy(strComps, firstComp);

    for (int i = 1; i < compCounter; i++)
    {
        char temp[10];
        sprintf(temp, " %d", comps[i]);
        strcat(strComps, temp);
    }
    
    int rc;
    char sql[1000];
    sprintf(sql, "INSERT INTO reserved (trainID, userID, comp, sits, count) VALUES (%d, %d, \'%s\', \'%s\', %d)", id, uid, strComps, strSits, (adult + child));
    char* zErrMsg = 0;
    rc = sqlite3_exec(db, sql, update_callback, 0, &zErrMsg);

    int newCapacity;
    newCapacity = capacity[i] - sitCounter;

    int trc;
    char tsql[1000];
    sprintf(tsql, "UPDATE trains set capacity = %d WHERE id is %d", newCapacity, id);
    char* tzErrMsg = 0;
    rc = sqlite3_exec(db, tsql, update_callback, 0, &tzErrMsg);

    return 1;
}

void user_input(int key, int* letterCount, char* field, int max) {

    if ((key >= 32) && (key <= 125) && (*letterCount < max))
    {
        *(field + *letterCount) = (char)key;
        (*letterCount)++;
    }

    if (IsKeyPressed(KEY_BACKSPACE))
    {
        (*letterCount)--;
        if (*letterCount < 0) *letterCount = 0;
        *(field + *letterCount) = '\0';
    }

}

/********************************************************************

    User Panel

********************************************************************/
void run_user_panel(const int screenWidth, const int screenHeight, int userid) {

    get_info(userid);

    /* Initializing Window */
    InitWindow(screenWidth, screenHeight, "Train Reservation");

    /* Images */
    Image add_image = LoadImage("Resources/mockup/images/add.png");
    Texture2D add = LoadTextureFromImage(add_image);
    UnloadImage(add_image);

    Image remove_image = LoadImage("Resources/mockup/images/remove.png");
    Texture2D remove = LoadTextureFromImage(remove_image);
    UnloadImage(remove_image);

    Image Train = LoadImage("Resources/mockup/images/splash_train.png");
    ImageResize(&Train, 72, 72);
    Texture2D traint = LoadTextureFromImage(Train);
    UnloadImage(Train);

    Image done_image = LoadImage("Resources/mockup/images/done.png");
    Texture2D done = LoadTextureFromImage(done_image);
    UnloadImage(done_image);

    /* Fonts */
    int base_font_size;
    if (screenWidth == 1920) base_font_size = 30;
    else base_font_size = 25;

    Font montserrat_regular = LoadFontEx("resources/fonts/montserrat.ttf", base_font_size, 0, 250);
    Font montserrat_small = LoadFontEx("resources/fonts/montserrat.ttf", 24, 0, 250);
    Font montserrat_big = LoadFontEx("resources/fonts/montserrat.ttf", base_font_size * 4 / 3, 0, 250);

    /* Rectangles */
    /* Containers */
    Rectangle window_container = { 50, 50, screenWidth * 5 / 7 , screenHeight - 100 };
    Rectangle option_list_container = { screenWidth - (screenWidth / 7) - 100, 50, screenWidth / 7, screenHeight - 100 };

    /* Options */
    Rectangle buy_ticket = { option_list_container.x + 20, option_list_container.y + 50, option_list_container.width - 40, 60 };
    Rectangle option_wallet = { option_list_container.x + base_font_size, option_list_container.y + base_font_size*5, 200, 60 };

    /* Buy Ticket Page */
    Rectangle oneway = { window_container.x + base_font_size * 7, window_container.y + base_font_size * 3, 25, 25 };
    Rectangle twoway = { window_container.x + base_font_size * 17, window_container.y + base_font_size * 3, 25, 25 };
    Rectangle oneway_container = { oneway.x - 10, oneway.y - 30, 50, 60 };
    Rectangle twoway_container = { twoway.x - 10, twoway.y - 30, 50, 60 };
    Rectangle from_input = { window_container.x + base_font_size * 10, window_container.y + base_font_size * 6.6, 200, 50 };
    Rectangle to_input = { window_container.x + base_font_size * 24, window_container.y + base_font_size * 6.6, 200, 50 };
    Rectangle add_adult_box = { window_container.x + base_font_size * 19.3, window_container.y + base_font_size * 19.8 - 15, 50, 50 };
    Rectangle add_children_box = { window_container.x + base_font_size * 27.3, window_container.y + base_font_size * 19.8 - 15, 50, 50 };
    Rectangle remove_adult_box = { window_container.x + base_font_size * 22.3, window_container.y + base_font_size * 19.8 - 15, 50, 50 };
    Rectangle remove_children_box = { window_container.x + base_font_size * 31.3, window_container.y + base_font_size * 19.8 - 15, 50, 50 };
    Rectangle full_comp = { window_container.x + base_font_size * 7, window_container.y + base_font_size * 21, 20, 20 };
    Rectangle full_comp_container = { full_comp.x, full_comp.y - 20 , 40, 40 };
    Rectangle raft_year_input = { window_container.x + base_font_size * 14, window_container.y + base_font_size * 11.6, 80, 40 };
    Rectangle raft_month_input = { window_container.x + base_font_size * 18, window_container.y + base_font_size * 11.6, 60, 40 };
    Rectangle raft_day_input = { window_container.x + base_font_size * 21.3, window_container.y + base_font_size * 11.6, 60, 40 };
    Rectangle return_year_input = { window_container.x + base_font_size * 14, window_container.y + base_font_size * 13.6, 80, 40 };
    Rectangle return_month_input = { window_container.x + base_font_size * 18, window_container.y + base_font_size * 13.6, 60, 40 };
    Rectangle return_day_input = { window_container.x + base_font_size * 21.3, window_container.y + base_font_size * 13.6, 60, 40 };
    Rectangle search_box = { window_container.x + window_container.width/2 - 80, window_container.y + window_container.height - 80, 160, 60 };
    Rectangle discount_input = { window_container.x + base_font_size * 25, window_container.y + base_font_size * 14.5, 230, 50 };
    Rectangle done_box = { discount_input.x + discount_input.width + 10, discount_input.y - 10, 50, 50 };
    Rectangle purchase = { window_container.x + window_container.width/2 - 100, window_container.y + window_container.height - 150, 200, 60 };

    /* Bought Ticket */
    Rectangle cover = { window_container.x + 100 , window_container.y + 100 , window_container.width - 200, window_container.height - 200 };

    /* Add money */
    Rectangle add_money = { window_container.x + base_font_size * 5 , window_container.y + base_font_size * 12 , 200, 50 };

    /* Is Clicked? */
    bool IC_buy_ticket = false;
    bool IC_show_trains = false;
    bool IC_one_way = true;
    bool IC_from_input = false;
    bool IC_to_input = false;
    bool IC_raft_year_input = false;
    bool IC_raft_month_input = false;
    bool IC_raft_day_input = false;
    bool IC_return_year_input = false;
    bool IC_return_month_input = false;
    bool IC_return_day_input = false;
    bool IC_full_comp = false;
    bool IC_discount_input = false;
    bool IC_bought = false;
    bool IC_wallet = false;

    int buy_train = -1;
    
    // strings and letter counters
    char from[100] = "";
    int from_lc = 0;
    char to[100] = "";
    int to_lc = 0;
    char raft_year[100] = "";
    int raft_year_lc = 0;
    char raft_month[100] = "";
    int raft_month_lc = 0;
    char raft_day[100] = "";
    int raft_day_lc = 0;
    char return_year[100] = " ";
    int return_year_lc = 0;
    char return_month[100] = " ";
    int return_month_lc = 0;
    char return_day[100] = " ";
    int return_day_lc = 0;
    char discount[100] = " ";
    int discount_lc = 0;

    int sum_price = 0;
    int sum_final_price = 0;
    char tempPath[100];
    char tempDate[100];
    char price[100];
    char final_price[100];

    /* passemgers counters */
    int adult_counter = 1;
    char adult_string[10];
    int children_counter = 0;
    char children_string[10];

    int position = 50;
    int t = -1;
    int disCon = 0;
    int purCon = -1;

    while (!WindowShouldClose())
    {
        BeginDrawing();

        ClearBackground(WHITE);

        DrawRectangleLinesEx(window_container, 5, myDarkBlue);
        DrawRectangleLinesEx(option_list_container, 5, myDarkBlue);
        DrawRectangleRec(buy_ticket, myRed);

        DrawTextEx(montserrat_big, "Buy Ticket", (Vector2) { buy_ticket.x + buy_ticket.width / 2 - base_font_size*3 , buy_ticket.y + buy_ticket.height / 2 - base_font_size*0.7}, montserrat_big.baseSize, 2, WHITE);
        DrawTextEx(montserrat_regular, "Wallet", (Vector2) { option_list_container.x + base_font_size , option_list_container.y + base_font_size*6}, montserrat_regular.baseSize, 2, myRed);

        DrawRectangleRec(option_wallet, myTransparent);

        /* Check if Buy Ticket Button is Clicked */
        if (CheckCollisionPointRec(GetMousePosition(), buy_ticket) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) 
        {
            IC_buy_ticket = true;
            IC_show_trains = false;
        }
        else if (!CheckCollisionPointRec(GetMousePosition(), window_container) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) IC_buy_ticket = false;

        /* If Buy Ticket Button was Clicked: */
        if (IC_buy_ticket) 
        {
            /* one way or two ways */
            {
                DrawTextEx(montserrat_regular, "One Way", (Vector2) { window_container.x + base_font_size * 8.5, window_container.y + base_font_size * 3 }, montserrat_regular.baseSize, 2, BLACK);
                DrawTextEx(montserrat_regular, "Two Ways", (Vector2) { window_container.x + base_font_size * 18.5, window_container.y + base_font_size * 3 }, montserrat_regular.baseSize, 2, BLACK);

                if (CheckCollisionPointRec(GetMousePosition(), oneway_container) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) IC_one_way = true;
                else if (CheckCollisionPointRec(GetMousePosition(), twoway_container) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) IC_one_way = false;

                DrawRectangleRec(oneway_container, myTransparent);
                DrawRectangleRec(twoway_container, myTransparent);
                if (IC_one_way)
                {
                    DrawRectangleRec(oneway, myRed);
                    DrawRectangleLinesEx(twoway, 3, myRed);
                }
                else
                {
                    DrawRectangleRec(twoway, myRed);
                    DrawRectangleLinesEx(oneway, 3, myRed);
                }
            }

            /* fill out form */
            {
                DrawRectangleLinesEx(from_input, 3, myRed);
                DrawTextEx(montserrat_regular, "From", (Vector2) { window_container.x + base_font_size * 7, window_container.y + base_font_size * 7 }, montserrat_regular.baseSize, 2, BLACK);
                DrawTextEx(montserrat_regular, from, (Vector2) { from_input.x + 10, from_input.y + 10 }, montserrat_regular.baseSize, 2, BLACK);
                
                DrawRectangleLinesEx(to_input, 3, myRed);
                DrawTextEx(montserrat_regular, "To", (Vector2) { window_container.x + base_font_size * 22, window_container.y + base_font_size * 7 }, montserrat_regular.baseSize, 2, BLACK);
                DrawTextEx(montserrat_regular, to, (Vector2) { to_input.x + 10, to_input.y + 10 }, montserrat_regular.baseSize, 2, BLACK);

                sprintf(adult_string, "%d", adult_counter);
                sprintf(children_string, "%d", children_counter);

                DrawTextEx(montserrat_regular, "Passengers", (Vector2) { window_container.x + base_font_size * 7, window_container.y + base_font_size * 19 }, montserrat_regular.baseSize, 2, BLACK);
                DrawTextEx(montserrat_regular, "Adults", (Vector2) { window_container.x + base_font_size * 20, window_container.y + base_font_size * 18 }, montserrat_regular.baseSize, 2, BLACK);
                DrawTextEx(montserrat_regular, "Children", (Vector2) { window_container.x + base_font_size * 28, window_container.y + base_font_size * 18 }, montserrat_regular.baseSize, 2, BLACK);
                DrawTextEx(montserrat_regular, adult_string, (Vector2) { window_container.x + base_font_size * 21.4, window_container.y + base_font_size * 20 }, montserrat_regular.baseSize, 2, BLACK);
                DrawTextEx(montserrat_regular, children_string, (Vector2) { window_container.x + base_font_size * 29.6, window_container.y + base_font_size * 20 }, montserrat_regular.baseSize, 2, BLACK);
                DrawTexture(add, window_container.x + base_font_size * 19.3, window_container.y +  base_font_size * 19.8, WHITE);
                DrawTexture(add, window_container.x + base_font_size * 27.3, window_container.y +  base_font_size * 19.8, WHITE);
                DrawTexture(remove, window_container.x + base_font_size * 22.3, window_container.y +  base_font_size * 19.8, WHITE);
                DrawTexture(remove, window_container.x + base_font_size * 31.3, window_container.y +  base_font_size * 19.8, WHITE);
                DrawRectangleRec(add_adult_box, myTransparent);
                DrawRectangleRec(add_children_box, myTransparent);
                DrawRectangleRec(remove_adult_box, myTransparent);
                DrawRectangleRec(remove_children_box, myTransparent);

                DrawTextEx(montserrat_regular, "Full Compartment", (Vector2) { full_comp.x + 30, full_comp.y - 1 }, montserrat_regular.baseSize, 2, BLACK);

                if (CheckCollisionPointRec(GetMousePosition(), from_input) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) IC_from_input = true;
                else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) IC_from_input = false;

                if (CheckCollisionPointRec(GetMousePosition(), to_input) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) IC_to_input = true;
                else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) IC_to_input = false;

                if (IC_from_input)
                {
                    DrawRectangleLinesEx(from_input, 3, myLightBlue);

                    int key = GetKeyPressed();
                    user_input(key, &from_lc, from, 13);
                }

                if (IC_to_input)
                {
                    DrawRectangleLinesEx(to_input, 3, myLightBlue);

int key = GetKeyPressed();
user_input(key, &to_lc, to, 13);
                }

                /* add or remove passenger */
                if (CheckCollisionPointRec(GetMousePosition(), add_adult_box) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) adult_counter++;
                if (CheckCollisionPointRec(GetMousePosition(), add_children_box) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) children_counter++;

                if (adult_counter > 1 && CheckCollisionPointRec(GetMousePosition(), remove_adult_box) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) adult_counter--;
                if (children_counter > 0 && CheckCollisionPointRec(GetMousePosition(), remove_children_box) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) children_counter--;

                if (CheckCollisionPointRec(GetMousePosition(), full_comp_container) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && IC_full_comp) IC_full_comp = false;
                else if (CheckCollisionPointRec(GetMousePosition(), full_comp_container) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) IC_full_comp = true;

                if (IC_full_comp)
                    DrawRectangleRec(full_comp, myRed);
                else
                DrawRectangleLinesEx(full_comp, 3, myRed);

                /* raft date */
                DrawTextEx(montserrat_regular, "Depart Date", (Vector2) { window_container.x + base_font_size * 7, window_container.y + base_font_size * 12 }, montserrat_regular.baseSize, 2, BLACK);
                DrawTextEx(montserrat_regular, raft_year, (Vector2) { raft_year_input.x + 10, raft_year_input.y + 8 }, montserrat_regular.baseSize, 2, BLACK);
                DrawTextEx(montserrat_regular, raft_month, (Vector2) { raft_month_input.x + 15, raft_month_input.y + 8 }, montserrat_regular.baseSize, 2, BLACK);
                DrawTextEx(montserrat_regular, raft_day, (Vector2) { raft_day_input.x + 15, raft_day_input.y + 8 }, montserrat_regular.baseSize, 2, BLACK);
                DrawRectangleLinesEx(raft_year_input, 3, myRed);
                DrawRectangleLinesEx(raft_month_input, 3, myRed);
                DrawRectangleLinesEx(raft_day_input, 3, myRed);

                if (CheckCollisionPointRec(GetMousePosition(), raft_year_input) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) IC_raft_year_input = true;
                else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) IC_raft_year_input = false;

                if (CheckCollisionPointRec(GetMousePosition(), raft_month_input) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) IC_raft_month_input = true;
                else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) IC_raft_month_input = false;

                if (CheckCollisionPointRec(GetMousePosition(), raft_day_input) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) IC_raft_day_input = true;
                else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) IC_raft_day_input = false;

                if (IC_raft_year_input)
                {
                    DrawRectangleLinesEx(raft_year_input, 3, myLightBlue);

                    int key = GetKeyPressed();
                    user_input(key, &raft_year_lc, raft_year, 4);
                }

                if (IC_raft_month_input)
                {
                    DrawRectangleLinesEx(raft_month_input, 3, myLightBlue);

                    int key = GetKeyPressed();
                    user_input(key, &raft_month_lc, raft_month, 2);
                }

                if (IC_raft_day_input)
                {
                    DrawRectangleLinesEx(raft_day_input, 3, myLightBlue);

                    int key = GetKeyPressed();
                    user_input(key, &raft_day_lc, raft_day, 2);
                }

                /* If Two ways: */
                if (!IC_one_way)
                {
                    DrawTextEx(montserrat_regular, "Return Date", (Vector2) { window_container.x + base_font_size * 7, window_container.y + base_font_size * 14 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawTextEx(montserrat_regular, return_year, (Vector2) { return_year_input.x + 10, return_year_input.y + 8 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawTextEx(montserrat_regular, return_month, (Vector2) { return_month_input.x + 15, return_month_input.y + 8 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawTextEx(montserrat_regular, return_day, (Vector2) { return_day_input.x + 15, return_day_input.y + 8 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawRectangleLinesEx(return_year_input, 3, myRed);
                    DrawRectangleLinesEx(return_month_input, 3, myRed);
                    DrawRectangleLinesEx(return_day_input, 3, myRed);

                    if (CheckCollisionPointRec(GetMousePosition(), return_year_input) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) IC_return_year_input = true;
                    else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) IC_return_year_input = false;

                    if (CheckCollisionPointRec(GetMousePosition(), return_month_input) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) IC_return_month_input = true;
                    else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) IC_return_month_input = false;

                    if (CheckCollisionPointRec(GetMousePosition(), return_day_input) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) IC_return_day_input = true;
                    else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) IC_return_day_input = false;

                    if (IC_return_year_input)
                    {
                        DrawRectangleLinesEx(return_year_input, 3, myLightBlue);

                        int key = GetKeyPressed();
                        user_input(key, &return_year_lc, return_year, 4);
                    }

                    if (IC_return_month_input)
                    {
                        DrawRectangleLinesEx(return_month_input, 3, myLightBlue);

                        int key = GetKeyPressed();
                        user_input(key, &return_month_lc, return_month, 2);
                    }

                    if (IC_return_day_input)
                    {
                        DrawRectangleLinesEx(return_day_input, 3, myLightBlue);

                        int key = GetKeyPressed();
                        user_input(key, &return_day_lc, return_day, 2);
                    }
                }
            }

            /* Search Trains */
            DrawRectangleLinesEx(search_box, 3, myDarkBlue);
            DrawTextEx(montserrat_big, "Search", (Vector2) { search_box.x + search_box.width/2 - base_font_size*2, search_box.y + search_box.height/2 - base_font_size*0.7 }, montserrat_big.baseSize, 2, myDarkBlue);

            if (CheckCollisionPointRec(GetMousePosition(), search_box) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) 
            {
                IC_buy_ticket = false;
                IC_show_trains= true;
                if (IC_one_way) search_oneway(from, to, raft_year, raft_month, raft_day, adult_counter, children_counter);
            }
        }

        /* If Show trains:: */
        if (IC_show_trains) 
        {
            Rectangle train_holder = { window_container.x + 50, window_container.y + position,window_container.width - 100, atrain_counter*200 };
            Rectangle top_scroll = { train_holder.x , train_holder.y - 50 , 100, 50 };
            Rectangle down_scroll = { train_holder.x , train_holder.y + train_holder.height + 50, 100, 50 };

            for (int i = 0; i < atrain_counter; i++) 
            {
                Rectangle buy_box = { train_holder.x + train_holder.width - 150, train_holder.y + (i * 200) + 90, 100, 50 };
                Rectangle top_visibility = { train_holder.x + train_holder.width - 150, train_holder.y + (i * 200) + 20, 100, 50 };
                Rectangle down_visibility = { train_holder.x + train_holder.width - 150, train_holder.y + (i * 200) + 180, 100, 50 };
                
                int k = 0;
                if ( (adult_counter + children_counter / capacity[i] < comps[i]) && CheckCollisionRecs(window_container, top_visibility) && CheckCollisionRecs(window_container, down_visibility))
                {
                    if (IC_full_comp)
                    {
                        if (adult_counter + children_counter <= per[i])
                        {
                            sum_price = (per[i] * adultfee[i]) - (children_counter * (adultfee[i] - childfee[i]));
                        }
                        else {
                            sum_price = ((((adult_counter + children_counter) / per[i]) + 1) * per[i] * adultfee[i]) - (children_counter * (adultfee[i] - childfee[i]));
                        }
                    }
                    else {
                        sum_price = (adult_counter * adultfee[i]) + (children_counter * childfee[i]);
                    }

                    sum_final_price = sum_price;

                    sprintf(tempPath, "%s ... %s", from, to);
                    sprintf(tempDate, "%s/%s/%s", raft_year, raft_month, raft_day);
                    sprintf(price, "Total Price: %d", sum_price);

                    DrawTexture(traint, train_holder.x + 50, train_holder.y + (i * 200) + 50, BLACK);
                    DrawTextEx(montserrat_regular, companies[i], (Vector2) { train_holder.x + 140, train_holder.y + (i * 200) + 70 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawTextEx(montserrat_regular, times[i], (Vector2) { train_holder.x + 50, train_holder.y + (i * 200) + 140 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawTextEx(montserrat_regular, tempPath, (Vector2) { train_holder.x + 400, train_holder.y + (i * 200) + 70 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawTextEx(montserrat_regular, tempDate, (Vector2) { train_holder.x + 170, train_holder.y + (i * 200) + 140 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawTextEx(montserrat_regular, price, (Vector2) { train_holder.x + 400, train_holder.y + (i * 200) + 140 }, montserrat_regular.baseSize, 2, BLACK);

                    DrawRectangleLinesEx(buy_box, 3, myDarkBlue);
                    DrawTextEx(montserrat_regular, "Buy", (Vector2) { buy_box.x + buy_box.width / 2 - base_font_size * 0.9, buy_box.y + buy_box.height / 2 - base_font_size * 0.5 }, montserrat_regular.baseSize, 2, BLACK);

                    if (CheckCollisionPointRec(GetMousePosition(), buy_box) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
                    {
                        IC_show_trains = false;
                        buy_train = i;
                    }
                }
            }

            int temp = GetMouseWheelMove();
            if (!CheckCollisionRecs(top_scroll, window_container) && !CheckCollisionRecs(down_scroll, window_container))
            {
                position -= (temp * 20);
            }
            if (CheckCollisionRecs(top_scroll, window_container) && !CheckCollisionRecs(down_scroll, window_container))
            {
                if ( temp > 0 )
                {
                    position -= (temp * 20);
                }
            }
            if (!CheckCollisionRecs(top_scroll, window_container) && CheckCollisionRecs(down_scroll, window_container))
            {
                if (temp < 0)
                {
                    position -= (temp * 20);
                }
            }
        }

        /* If a train was chosen */
        if (buy_train != -1)
        {
            t = buy_train;
            sprintf(final_price, "Price: %d", sum_final_price);

            DrawTextEx(montserrat_regular, "Company:", (Vector2) { window_container.x + base_font_size*3, window_container.y + base_font_size*3 }, montserrat_regular.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, companies[buy_train], (Vector2) { window_container.x + base_font_size * 8, window_container.y + base_font_size*3 }, montserrat_regular.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, "Name:", (Vector2) { window_container.x + base_font_size * 3, window_container.y + base_font_size*6 }, montserrat_regular.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, names[buy_train], (Vector2) { window_container.x + base_font_size * 6.5, window_container.y + base_font_size*6 }, montserrat_regular.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, "From:", (Vector2) { window_container.x + base_font_size * 3, window_container.y + base_font_size * 9 }, montserrat_regular.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, from, (Vector2) { window_container.x + base_font_size * 6, window_container.y + base_font_size * 9 }, montserrat_regular.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, "To:", (Vector2) { window_container.x + base_font_size * 3, window_container.y + base_font_size * 12 }, montserrat_regular.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, to, (Vector2) { window_container.x + base_font_size * 4.8, window_container.y + base_font_size * 12 }, montserrat_regular.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, final_price, (Vector2) { window_container.x + base_font_size * 3, window_container.y + base_font_size * 15 }, montserrat_regular.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, "Discount Code:", (Vector2) { window_container.x + base_font_size * 17, window_container.y + base_font_size * 15 }, montserrat_regular.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, discount, (Vector2) { discount_input.x + 10, discount_input.y + 13}, montserrat_regular.baseSize, 2, BLACK);

            DrawRectangleLinesEx(discount_input, 3, myDarkBlue);
            DrawTexture(done, discount_input.x + discount_input.width + 10, discount_input.y + 5, WHITE);

            DrawRectangleRec(purchase, myRed);
            DrawTextEx(montserrat_big, "Purchase", (Vector2) { purchase.x + purchase.width/2 - base_font_size*2.7, purchase.y + purchase.height - base_font_size*1.8 }, montserrat_big.baseSize, 2, WHITE);

            if (CheckCollisionPointRec(GetMousePosition(), done_box) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
            {
                int temp = add_discount(discount);

                if (temp != -1 && disCon == 0)
                {
                    sum_final_price = sum_final_price * temp / 100;
                    disCon = 1;
                }
            }

            if (CheckCollisionPointRec(GetMousePosition(), discount_input) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) IC_discount_input = true;
            else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) IC_discount_input = false;

            if (IC_discount_input)
            {
                DrawRectangleLinesEx(discount_input, 4, myDarkBlue);

                int key = GetKeyPressed();
                user_input(key, &discount_lc, discount, 15);
            }

            if (CheckCollisionPointRec(GetMousePosition(), purchase) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
            {
                purCon = purchase_ticket(buy_train, userid, ids[buy_train], adult_counter, children_counter, sum_final_price, IC_full_comp);
                search_oneway(from, to, raft_year, raft_month, raft_day, adult_counter, children_counter);
                if (purCon == 1)
                {
                    IC_bought = true;
                    buy_train = -1;
                }
                else {
                    IC_wallet = true;
                    buy_train = -1;
                }
            }            
        }

        if (IC_bought)
        {
            if (IC_bought && !CheckCollisionPointRec(GetMousePosition(), window_container) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) IC_bought = false;

            char temp[10];
            sprintf(temp, "%d", ids[t]);
            DrawRectangleLinesEx(cover, 3, myDarkBlue);
            DrawTextEx(montserrat_big, "Ticket", (Vector2) { cover.x + cover.width/2 - base_font_size*2, cover.y + 40 }, montserrat_big.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, "train id:", (Vector2) { cover.x + base_font_size*5 , cover.y + 200 }, montserrat_regular.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, "compartments:", (Vector2) { cover.x + base_font_size*5 , cover.y + 270 }, montserrat_regular.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, "sits:", (Vector2) { cover.x + base_font_size*5 , cover.y + 340 }, montserrat_regular.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, temp, (Vector2) { cover.x + base_font_size * 9, cover.y + 200 }, montserrat_regular.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, strComps, (Vector2) { cover.x + base_font_size * 13, cover.y + 270 }, montserrat_regular.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, strSits, (Vector2) { cover.x + base_font_size * 8, cover.y + 340 }, montserrat_regular.baseSize, 2, BLACK);
        }

        if (CheckCollisionPointRec(GetMousePosition(), option_wallet) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
        {
            IC_wallet = true;
        }

        if (IC_wallet)
        {
            if (!CheckCollisionPointRec(GetMousePosition(), option_wallet) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) IC_wallet = false;
            DrawTextEx(montserrat_regular, "Current Money:", (Vector2) { window_container.x + base_font_size*5, window_container.y + base_font_size*10 }, montserrat_regular.baseSize, 2, BLACK);
            
            char temp[10];
            sprintf(temp, "%d", money);
            DrawTextEx(montserrat_regular, temp, (Vector2) { window_container.x + base_font_size * 13, window_container.y + base_font_size * 10 }, montserrat_regular.baseSize, 2, BLACK);
            
            DrawRectangleRec(add_money, myRed);
            DrawTextEx(montserrat_regular, "Add", (Vector2) { add_money.x + 70, add_money.y + 10 }, montserrat_regular.baseSize, 2, WHITE);
        
            if (CheckCollisionPointRec(GetMousePosition(), option_wallet) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
            {

            }
        }

        EndDrawing();
    }

    UnloadTexture(add);
    UnloadTexture(remove);

    CloseWindow();
}