#ifndef _MODIFY_TRAIN_H_
#define _MODIFY_TRAIN_H_

int train_modify(int id, char name[100], char origin[100], char dest[100], char year[4], char month[2], char day[2], char hour[2], char min[2], char master[100], char driver[100], char afee[100], char cfee[100], char comps[100], char capa[100]);

int train_add(char co[100], char name[100], char origin[100], char dest[100], char year[100], char month[100], char day[100], char hour[100], char min[100], char master[100], char driver[100], char afee[100], char cfee[100], char comps[100], char capa[100]);

int train_delete(int id);

#endif 
