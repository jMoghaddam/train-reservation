#include "raylib.h"
#include <time.h> 
#include "splash.h"
#include "login.h"
#include "signup.h"
#include "user_panel.h"
#include "admin_panel.h"

#define myDarkBlue CLITERAL{36, 48, 94, 255}
#define myLightBlue CLITERAL{55, 71, 133, 255}
#define mySkyBlue CLITERAL{168, 208, 230, 255}
#define myRed CLITERAL{247, 108, 108, 255}
#define myYellow CLITERAL{248, 233, 161, 255}

int main(void)
{

    const int screenWidth = 1920;
    const int screenHeight = 1080;

    run_splash(screenWidth, screenHeight);

    run_login(screenWidth, screenHeight);

    return 0;
}