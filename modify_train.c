#include <stdio.h>
#include <string.h>
#include "sqlite3.h"
#include "raylib.h"
#include "check_login.h"

#pragma warning(disable: 4996)

sqlite3* db;

int train_modify_callback(void* NotUsed, int argc, char** argv, char** azColName) {
    return 0;
}

int train_modify(int id, char name[100], char origin[100], char dest[100], char year[4], char month[2], char day[2], char hour[2], char min[2], char master[100], char driver[100], char afee[100], char cfee[100], char comps[100], char capa[100]) {

    if (!connect()) {
        return -1;
    }

    char date[20];
    char time[20];

    sprintf(date, "%s/%s/%s", year, month, day);
    sprintf(time, "%s:%s", hour, min);

    int tempafee = TextToInteger(afee);
    int tempcfee = TextToInteger(cfee);
    int tempcomps = TextToInteger(comps);
    int tempcapaper = TextToInteger(capa);
    int tempcapa = tempcapaper * tempcomps;

    int urc;
    char usql[1000];
    sprintf(usql, "UPDATE trains set name = \'%s\', origin = \'%s\', destination =\'%s\', date = \'%s\', time = \'%s\', trainmaster = \'%s\', driver = \'%s\', adultfee = %d, childfee = %d, compartments = %d, capacity = %d, CapaPerComp = %d, allComps = %d WHERE id is %d", name, origin, dest, date, time, master, driver, tempafee, tempcfee, tempcomps, tempcapa, tempcapaper, tempcomps, id);
    char* uzErrMsg = 0;

    urc = sqlite3_exec(db, usql, train_modify_callback, 0, &uzErrMsg);

    if (urc != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", uzErrMsg);
        sqlite3_free(uzErrMsg);
        return -1;
    }
    
}

int train_add(char co[100], char name[100], char origin[100], char dest[100], char year[100], char month[100], char day[100], char hour[100], char min[100], char master[100], char driver[100], char afee[100], char cfee[100], char comps[100], char capa[100]) {

    if (!connect()) {
        return -1;
    }

    char date[20];
    char time[20];

    sprintf(date, "%s/%s/%s", year, month, day);
    sprintf(time, "%s:%s", hour, min);

    int tempafee = TextToInteger(afee);
    int tempcfee = TextToInteger(cfee);
    int tempcomps = TextToInteger(comps);
    int tempcapapercomp = TextToInteger(capa);
    int tempcapa = tempcomps * tempcapapercomp;

    int urc;
    char usql[1000];
    sprintf(usql, "INSERT INTO trains (company, name, origin, destination, date, time, trainmaster, driver, adultfee, childfee, compartments, capacity, CapaPerComp) VALUES (\'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', %d, %d, %d, %d, %d)",co, name, origin, dest, date, time, master, driver, tempafee, tempcfee, tempcomps, tempcapa, tempcapapercomp);
    char* uzErrMsg = 0;

    urc = sqlite3_exec(db, usql, train_modify_callback, 0, &uzErrMsg);

    if (urc != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", uzErrMsg);
        sqlite3_free(uzErrMsg);
        return -1;
    }

}

int train_delete(int id) {

    if (!connect()) {
        return -1;
    }

    int urc;
    char usql[1000];
    sprintf(usql, "DELETE FROM trains WHERE id is %d", id);
    char* uzErrMsg = 0;

    urc = sqlite3_exec(db, usql, train_modify_callback, 0, &uzErrMsg);

    if (urc != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", uzErrMsg);
        sqlite3_free(uzErrMsg);
        return -1;
    }

}