#include <stdio.h>
#include <string.h>
#include "raylib.h"
#include "sqlite3.h"
#include "check_login.h"
#include "modify_co.h"
#include "modify_train.h"

#pragma warning(disable: 4996)

#define myDarkBlue CLITERAL{36, 48, 94, 255}
#define myLightBlue CLITERAL{55, 71, 133, 255}
#define mySkyBlue CLITERAL{168, 208, 230, 255}
#define myRed CLITERAL{247, 108, 108, 255}
#define myYellow CLITERAL{248, 233, 161, 255}
#define myTransparent CLITERAL{0, 0, 0, 0}

// initializing database
sqlite3* db;

// list of company names, ( long problem fixed ) company names, id of company names
char co_names[100][100];
char show_co_names[100][100];
int co_ids[100];

// list of train infos
int train_ids[100];
char train_names[100][100];
char show_train_names[100][100];
char origins[100][100];
char destination[100][100];
char years[100][100];
char months[100][100];
char days[100][100];
char hours[100][100];
char mins[100][100];
char masters[100][100];
char drivers[100][100];
char afees[100][100];
char cfees[100][100];
char comps[100][100];
char capas[100][100];
char capapers[100][100];

// How many company names do we have?
int co_counter = 0;

// How many train names do we have?
int train_counter = 0;

// this is co_list() callback functions
int co_callback(void* NotUsed, int argc, char** argv, char** azColName) {

    strcpy(co_names[co_counter], argv[1]);
    strcpy(show_co_names[co_counter], argv[1]);
    co_ids[co_counter] = atoi(argv[0]);

    co_counter++;

    return 0;

}

// this is train_list() callback functions
int empty_list = 0;
int train_callback(void* NotUsed, int argc, char** argv, char** azColName) {

    empty_list = 1;

    train_ids[train_counter] = atoi(argv[0]);
    strcpy(train_names[train_counter], argv[2]);
    strcpy(show_train_names[train_counter], argv[2]);
    strcpy(origins[train_counter], argv[3]);
    strcpy(destination[train_counter], argv[4]);

    for (int i = 0; i < 4; i++)
    {
        years[train_counter][i] = argv[5][i];
    }

    for (int i = 0; i < 2; i++)
    {
        months[train_counter][i] = argv[5][i + 5];
    }

    for (int i = 0; i < 2; i++)
    {
        days[train_counter][i] = argv[5][i + 9];
    }

    for (int i = 0; i < 2; i++)
    {
        hours[train_counter][i] = argv[6][i];
    }

    for (int i = 0; i < 2; i++)
    {
        mins[train_counter][i] = argv[6][i + 3];
    }

    strcpy(masters[train_counter], argv[7]);
    strcpy(drivers[train_counter], argv[8]);

    strcpy(afees[train_counter], argv[9]);
    strcpy(cfees[train_counter], argv[10]);
    strcpy(comps[train_counter], argv[11]);
    strcpy(capas[train_counter], argv[12]);
    strcpy(capapers[train_counter], argv[13]);

    train_counter++;

    return 0;

}

// this function gets a list of company names
int co_list() {
    
    if (!connect()) {
        return 0;
    }

    int rc;
    char sql[1000] = "SELECT * FROM companies";
    char* zErrMsg = 0;

    rc = sqlite3_exec(db, sql, co_callback, 0, &zErrMsg);

    if (rc != SQLITE_OK) {
        fprintf(stderr, "SQL initial_db_error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        return -1;
    }

    return 1;

}

// this function gets a list of train names
int train_list(int i) {

    if (!connect()) {
        return 0;
    }

    int rc;
    char sql[1000];
    sprintf(sql, "SELECT * FROM trains WHERE company = \'%s\'", co_names[i]);
    char* zErrMsg = 0;

    rc = sqlite3_exec(db, sql, train_callback, 0, &zErrMsg);

    if (empty_list == 0) {
        for (int i = 0; i < train_counter; i++) {
            strcpy(show_train_names[i], " ");
        }
    }

    empty_list = 0;

    if (rc != SQLITE_OK) {
        fprintf(stderr, "SQL initial_db_error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        return -1;
    }

    return 1;

}

// this function fixes long names
void co_name_cutter() {

    int max;
    if (GetScreenWidth == 1920) max = 12;
    else max = 9;
    
    for (int i = 0; i < co_counter; i++) {
        if (strlen(co_names[i]) > max) {
            for (int j = max; j < max + 3; j++) {
                show_co_names[i][j] = '.';
            }
            show_co_names[i][max + 3] = '\0';
        }
    }

}

// this function fixes long names
void train_name_cutter() {

    int max;
    if (GetScreenWidth == 1920) max = 20;
    else max = 15;

    for (int i = 0; i < train_counter; i++) {
        if (strlen(train_names[i]) > max) {
            for (int j = max; j < max + 3; j++) {
                show_train_names[i][j] = '.';
            }
            show_train_names[i][max + 3] = '\0';
        }
    }

}

// this function searches a list
void search_co(char x[100]) {

    if (x[0] == '\0') {
        for (int i = 0; i < co_counter; i++) {
            strcpy(show_co_names[i], co_names[i]);
            co_name_cutter();
        }
    }
    else {
        for (int i = 0; i < co_counter; i++) {
            int j = 0;
            int c = 0;
            while (x[j] != '\0' && (x[j] == co_names[i][j] || x[j] == co_names[i][j] + 32 )) {
                c++;
                j++;
            }

            if (c != strlen(x)) {
                strcpy(show_co_names[i], " ");
            }
        }
    }

}

// this function searches a list
void search_train(char name[100], char origin[100], char dest[100], char year[100], char month[100], char day[100], char hour[100], char min[100], char master[100], char driver[100], char afee[100], char cfee[100], char comp[100], char capa[100]) {

    for (int i = 0; i < train_counter; i++) {
        strcpy(show_train_names[i], train_names[i]);
        train_name_cutter();
    }

    if (name[0] != ' ') {
        
        for (int i = 0; i < train_counter; i++) {
            int j = 0;
            int c = 0;
            while (name[j] != '\0' && (name[j] == train_names[i][j] || name[j] == train_names[i][j] + 32)) {
                c++;
                j++;
            }

            if (c != strlen(name)) {
                strcpy(show_train_names[i], " ");
            }
        }

    }

    if (origin[0] != ' ') {

        for (int i = 0; i < train_counter; i++) {
            int j = 0;
            int c = 0;
            while (origin[j] != '\0' && (origin[j] == origins[i][j] || origin[j] == origins[i][j] + 32)) {
                c++;
                j++;
            }

            if (c != strlen(origin)) {
                strcpy(show_train_names[i], " ");
            }
        }

    }

    if (dest[0] != ' ') {

        for (int i = 0; i < train_counter; i++) {
            int j = 0;
            int c = 0;
            while (dest[j] != '\0' && (dest[j] == destination[i][j] || dest[j] == destination[i][j] + 32)) {
                c++;
                j++;
            }

            if (c != strlen(dest)) {
                strcpy(show_train_names[i], " ");
            }
        }

    }

    if (year[0] != ' ') {

        for (int i = 0; i < train_counter; i++) {
            int j = 0;
            int c = 0;
            while (year[j] != '\0' && (year[j] == years[i][j] || year[j] == years[i][j] + 32)) {
                c++;
                j++;
            }

            if (c != strlen(year)) {
                strcpy(show_train_names[i], " ");
            }
        }

    }

    if (month[0] != ' ') {

        for (int i = 0; i < train_counter; i++) {
            int j = 0;
            int c = 0;
            while (month[j] != '\0' && (month[j] == months[i][j] || month[j] == months[i][j] + 32)) {
                c++;
                j++;
            }

            if (c != strlen(month)) {
                strcpy(show_train_names[i], " ");
            }
        }

    }

    if (day[0] != ' ') {

        for (int i = 0; i < train_counter; i++) {
            int j = 0;
            int c = 0;
            while (day[j] != '\0' && (day[j] == days[i][j] || day[j] == days[i][j] + 32)) {
                c++;
                j++;
            }

            if (c != strlen(day)) {
                strcpy(show_train_names[i], " ");
            }
        }

    }

    if (hour[0] != ' ') {

        for (int i = 0; i < train_counter; i++) {
            int j = 0;
            int c = 0;
            while (hour[j] != '\0' && (hour[j] == hours[i][j] || hour[j] == hours[i][j] + 32)) {
                c++;
                j++;
            }

            if (c != strlen(hour)) {
                strcpy(show_train_names[i], " ");
            }
        }

    }

    if (min[0] != ' ') {

        for (int i = 0; i < train_counter; i++) {
            int j = 0;
            int c = 0;
            while (min[j] != '\0' && (min[j] == mins[i][j] || min[j] == mins[i][j] + 32)) {
                c++;
                j++;
            }

            if (c != strlen(min)) {
                strcpy(show_train_names[i], " ");
            }
        }

    }

    if (master[0] != ' ') {

        for (int i = 0; i < train_counter; i++) {
            int j = 0;
            int c = 0;
            while (master[j] != '\0' && (master[j] == masters[i][j] || master[j] == masters[i][j] + 32)) {
                c++;
                j++;
            }

            if (c != strlen(master)) {
                strcpy(show_train_names[i], " ");
            }
        }

    }

    if (driver[0] != ' ') {

        for (int i = 0; i < train_counter; i++) {
            int j = 0;
            int c = 0;
            while (driver[j] != '\0' && (driver[j] == drivers[i][j] || driver[j] == drivers[i][j] + 32)) {
                c++;
                j++;
            }

            if (c != strlen(driver)) {
                strcpy(show_train_names[i], " ");
            }
        }

    }

    if (afee[0] != ' ') {

        for (int i = 0; i < train_counter; i++) {
            int j = 0;
            int c = 0;
            while (afee[j] != '\0' && (afee[j] == afees[i][j] || afee[j] == afees[i][j] + 32)) {
                c++;
                j++;
            }

            if (c != strlen(afee)) {
                strcpy(show_train_names[i], " ");
            }
        }

    }

    if (cfee[0] != ' ') {

        for (int i = 0; i < train_counter; i++) {
            int j = 0;
            int c = 0;
            while (cfee[j] != '\0' && (cfee[j] == cfees[i][j] || cfee[j] == cfees[i][j] + 32)) {
                c++;
                j++;
            }

            if (c != strlen(cfee)) {
                strcpy(show_train_names[i], " ");
            }
        }

    }

    if (comp[0] != ' ') {

        for (int i = 0; i < train_counter; i++) {
            int j = 0;
            int c = 0;
            while (comp[j] != '\0' && (comp[j] == comps[i][j] || comp[j] == comps[i][j] + 32)) {
                c++;
                j++;
            }

            if (c != strlen(comp)) {
                strcpy(show_train_names[i], " ");
            }
        }

    }

    if (capa[0] != ' ') {

        for (int i = 0; i < train_counter; i++) {
            int j = 0;
            int c = 0;
            while (capa[j] != '\0' && (capa[j] == capas[i][j] || capa[j] == capas[i][j] + 32)) {
                c++;
                j++;
            }

            if (c != strlen(capa)) {
                strcpy(show_train_names[i], " ");
            }
        }

    }

}

void input(int key, int *letterCount, char *field, int max) {

    if ((key >= 32) && (key <= 125) && (*letterCount < max))
    {
        *(field + *letterCount) = (char)key;
       (*letterCount)++;
    }

    if (IsKeyPressed(KEY_BACKSPACE))
    {
        (*letterCount)--;
        if (*letterCount < 0) *letterCount = 0;
        *(field + *letterCount) = '\0';
    }

}

void run_admin_panel(const int screenWidth, const int screenHeight) {

    // Initializing Window
    InitWindow(screenWidth, screenHeight, "Train Reservation");

    // Images
    Image search_image = LoadImage("Resources/mockup/images/search.png");
    Texture2D co_searcht = LoadTextureFromImage(search_image);
    UnloadImage(search_image);

    Image done_image = LoadImage("Resources/mockup/images/done.png");
    Texture2D done = LoadTextureFromImage(done_image);
    UnloadImage(done_image);

    Image delete_image = LoadImage("Resources/mockup/images/delete.png");
    Texture2D delete = LoadTextureFromImage(delete_image);
    UnloadImage(delete_image);
    
    // Fonts
    int base_font_size;
    if (screenWidth == 1920) base_font_size = 30;
    else base_font_size = 25;

    Font montserrat_regular = LoadFontEx("resources/fonts/montserrat.ttf", base_font_size, 0, 250);
    Font montserrat_small = LoadFontEx("resources/fonts/montserrat.ttf", 24, 0, 250);
    Font montserrat_big = LoadFontEx("resources/fonts/montserrat.ttf", 48, 0, 250);

    // Boxes Declaration
    // List of comapnies
    Rectangle co_list_container = { screenWidth - (screenWidth / 7) - 50, 50, screenWidth / 7, screenHeight - 100 };
    Rectangle window_container = { 50, 50, screenWidth - (screenWidth * 12 / 35) - 200, screenHeight - 100 };
    Rectangle input_co_name = { window_container.width / 2 - window_container.width / 6 , window_container.height / 2 + 50, window_container.width / 2, 50 };
    Rectangle input_search_co_name = { co_list_container.x , co_list_container.y, co_list_container.width, 50 };
    Rectangle btn_co_search = { co_list_container.x , co_list_container.y - 20, 50, 70 };
    Rectangle btn_submit_co = { input_co_name.x , input_co_name.y + 75, input_co_name.width / 3, 50 };
    Rectangle btn_delete_co = { btn_submit_co.x + btn_submit_co.width + 30 , input_co_name.y + 75, input_co_name.width / 3, 50 };
    Rectangle btn_new_co = { co_list_container.x + 20 , co_list_container.y + co_list_container.height - 60 , co_list_container.width - 40, 50 };
    Rectangle btn_add_co = { input_co_name.x , input_co_name.y + 75, input_co_name.width / 3, 50 };
    Rectangle btn_cancel_co = { btn_submit_co.x + btn_submit_co.width + 30 , input_co_name.y + 75, input_co_name.width / 3, 50 };

    // list of trains
    Rectangle train_list_container = { co_list_container.x - (screenWidth / 5) - 50, 50, screenWidth / 5, screenHeight - 100 };
    Rectangle train_window_container = { 50, 50, screenWidth - (screenWidth * 12 / 35) - 200, screenHeight - 100 };
    Rectangle btn_new_train = { train_list_container.x + 20 , train_list_container.y + train_list_container.height - 60 , train_list_container.width - 40, 50 };
    Rectangle input_search_train_name = { train_list_container.x ,train_list_container.y, train_list_container.width, 50 };
    Rectangle btn_train_search = {train_list_container.x , train_list_container.y - 20, train_list_container.width, 70 };
    Rectangle add_train_name = { window_container.x + window_container.width / 2 - base_font_size*5, window_container.y + window_container.height / 2 - base_font_size * 14.8, 350, 50 };
    Rectangle add_train_origin = { window_container.x + window_container.width / 2 - base_font_size * 5, window_container.y + window_container.height / 2 - base_font_size * 12.3, 350, 50 };
    Rectangle add_train_destination = { window_container.x + window_container.width / 2 - base_font_size * 5, window_container.y + window_container.height / 2 - base_font_size * 9.8, 350, 50 };
    Rectangle add_train_year = { window_container.x + window_container.width / 2 - base_font_size * 5, window_container.y + window_container.height / 2 - base_font_size * 7.3, 120, 40 };
    Rectangle add_train_month = { window_container.x + window_container.width / 2 + base_font_size * 1.5, window_container.y + window_container.height / 2 - base_font_size * 7.3, 60, 40 };
    Rectangle add_train_day = { window_container.x + window_container.width / 2 + base_font_size * 5.5, window_container.y + window_container.height / 2 - base_font_size * 7.3, 60, 40 };
    Rectangle add_train_min = { window_container.x + window_container.width / 2 + base_font_size * 5.5, window_container.y + window_container.height / 2 - base_font_size * 4.8, 60, 40 };
    Rectangle add_train_hour = { window_container.x + window_container.width / 2 + base_font_size * 1.5, window_container.y + window_container.height / 2 - base_font_size * 4.8, 60, 40 };
    Rectangle add_train_master = { window_container.x + window_container.width / 2 - base_font_size * 3, window_container.y + window_container.height / 2 + base_font_size * 1.7, 300, 40 };
    Rectangle add_train_driver = { window_container.x + window_container.width / 2 - base_font_size * 3, window_container.y + window_container.height / 2 + base_font_size * 4.2, 300, 40 };
    Rectangle add_train_afee = { window_container.x + window_container.width / 2 - base_font_size * 9, window_container.y + window_container.height / 2 + base_font_size * 7.9, 200, 40 };
    Rectangle add_train_cfee = { window_container.x + window_container.width / 2 - base_font_size * 9, window_container.y + window_container.height / 2 + base_font_size * 10.4, 200, 40 };
    Rectangle add_train_comps = { window_container.x + window_container.width / 2 + base_font_size * 9, window_container.y + window_container.height / 2 + base_font_size * 7.9, 100, 40 };
    Rectangle add_train_capa = { window_container.x + window_container.width / 2 + base_font_size * 9, window_container.y + window_container.height / 2 + base_font_size * 10.4, 100, 40 };
    Rectangle done_container = { window_container.x + window_container.width / 2 - base_font_size * 2, window_container.y + window_container.height - base_font_size*2.3, 40, 40 };
    Rectangle delete_container = { window_container.x + window_container.width / 2, window_container.y + window_container.height - base_font_size*2.3, 40, 40 };

    // Are buttons and boxes clicked?
    bool isClicked_input_co_name = false;
    bool isClicked_btn_new_co = false;
    bool isClicked_btn_new_train = false;
    bool isClicked_input_new_co = false;
    bool isClicked_input_search_co_name = false;
    bool isClicked_btn_search_co = false;
    bool isClicked_input_train_name = false;
    bool isClicked_input_train_origin = false;
    bool isClicked_input_train_destination = false;
    bool isClicked_input_train_year = false;
    bool isClicked_input_train_month = false;
    bool isClicked_input_train_day = false;
    bool isClicked_input_train_hour = false;
    bool isClicked_input_train_min = false;
    bool isClicked_input_train_master = false;
    bool isClicked_input_train_driver = false;
    bool isClicked_input_train_afee = false;
    bool isClicked_input_train_cfee = false;
    bool isClicked_input_train_comps = false;
    bool isClicked_input_train_capa = false;

    bool isClicked_input_train_name_holder = false;
    bool isClicked_input_train_origin_holder = false;
    bool isClicked_input_train_destination_holder = false;
    bool isClicked_input_train_year_holder = false;
    bool isClicked_input_train_month_holder = false;
    bool isClicked_input_train_day_holder = false;
    bool isClicked_input_train_hour_holder = false;
    bool isClicked_input_train_min_holder = false;
    bool isClicked_input_train_master_holder = false;
    bool isClicked_input_train_driver_holder = false;
    bool isClicked_input_train_afee_holder = false;
    bool isClicked_input_train_cfee_holder = false;
    bool isClicked_input_train_comps_holder = false;
    bool isClicked_input_train_capa_holder = false;

    bool isClicked_input_search_train_name = false;
    bool isClicked_btn_search_train = false;

    // Which company is selected?
    int mouse_on_co_cb = -1;

    // Which train is selected?
    int mouse_on_train_cb = -1;

    // strings and letter counters
    char initial_db_error[100] = " ";
    char co_name_holder[100] = " ";
    int co_name_holder_letterCount = 0;
    char input_errors[10] = " ";
    char co_search[] = "search...";
    char co_search_holder[100] = " ";
    int co_search_holder_letterCount = 0;
    char train_search[] = "search...";
    char train_search_holder[100] = "";
    int train_search_holder_letterCount = 0;

    char train_name_holder[100] = " ";
    int train_name_holder_counter = 0;
    char train_origin_holder[100] = " ";
    int train_origin_holder_counter = 0;
    char train_dest_holder[100] = " ";
    int train_dest_holder_counter = 0;
    char train_year_holder[100] = " ";
    int train_year_holder_counter = 0;
    char train_month_holder[100] = " ";
    int train_month_holder_counter = 0;
    char train_day_holder[100] = " ";
    int train_day_holder_counter = 0;
    char train_hour_holder[100] = " ";
    int train_hour_holder_counter = 0;
    char train_min_holder[100] = " ";
    int train_min_holder_counter = 0;
    char train_master_holder[100] = " ";
    int train_master_holder_counter = 0;
    char train_driver_holder[100] = " ";
    int train_driver_holder_counter = 0;
    char train_afee_holder[100] = " ";
    int train_afee_holder_counter = 0;
    char train_cfee_holder[100] = " ";
    int train_cfee_holder_counter = 0;
    char train_comps_holder[100] = " ";
    int train_comps_holder_counter = 0;
    char train_capa_holder[100] = " ";
    int train_capa_holder_counter = 0;

    int temp = -1;
    int filter = -1;

    // get the list of existing companies from db
    if (co_list() != 1) {
        strcpy(initial_db_error, "error connecting to database");
    }

    // fix long names
    co_name_cutter();

    // loop condition
    int con = 1;
    SetTargetFPS(60);
    while (!WindowShouldClose() && con == 1)
    {

        BeginDrawing();

        // Background Image
        ClearBackground(WHITE);

        // co list 
        {
            // container
            DrawRectangleLinesEx(co_list_container, 3, myDarkBlue);

            // add company button
            DrawRectangleRec(btn_new_co, myRed);
            DrawTextEx(montserrat_regular, "add", (Vector2) { btn_new_co.x + btn_new_co.width / 2 - base_font_size + 7, btn_new_co.y + btn_new_co.height/2 - base_font_size/2 }, montserrat_regular.baseSize, 2, WHITE);

            // search box
            {
                DrawTexture(co_searcht, input_search_co_name.x + 10, input_search_co_name.y + 6, WHITE);

                DrawRectangleLinesEx(input_search_co_name, 3, myDarkBlue);
                DrawRectangleRec(btn_co_search, myTransparent);
                DrawTextEx(montserrat_small, co_search, (Vector2) { input_search_co_name.x + 60, input_search_co_name.y + 13 }, montserrat_small.baseSize, 2, GRAY);
                if (co_search_holder[0] != '\0') {
                    DrawTextEx(montserrat_small, co_search_holder, (Vector2) { input_search_co_name.x + 60, input_search_co_name.y + 13 }, montserrat_small.baseSize, 2, myRed);

                }

                // check if search box is clicked
                if (CheckCollisionPointRec(GetMousePosition(), input_search_co_name) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_search_co_name = true;
                else if (!CheckCollisionPointRec(GetMousePosition(), btn_co_search) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_search_co_name = false;

                // if search box was clicked:
                if (isClicked_input_search_co_name) {
                    strcpy(co_search, " ");

                    DrawRectangleLinesEx(input_search_co_name, 4, myDarkBlue);

                    int key = GetKeyPressed();

                    int max;
                    if (screenWidth == 1920) max = 14;
                    else max = 9;

                    input(key, &co_search_holder_letterCount, co_search_holder, max);

                }
                else {
                    if ( co_search_holder[0] == '\0') strcpy(co_search, "search...");
                }

                if (CheckCollisionPointRec(GetMousePosition(), btn_co_search) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_btn_search_co = true;
                else if (!IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_btn_search_co = false;

                // if search button was clicked:
                if (isClicked_btn_search_co) {
                    // this function searches in the list
                    search_co(co_search_holder);
                    
                }
            }
            
            // show company names loop
            int j = 0;
            for (int i = 0; i < co_counter; i++) {

                if (strcmp(show_co_names[i], " ") != 0) {
                    // print all of names in the list
                    DrawTextEx(montserrat_regular, show_co_names[i], (Vector2) { co_list_container.x + 10, co_list_container.y + (j * 50) + 70 }, montserrat_regular.baseSize, 2, BLACK);
                    
                    // put checkbox in front of names
                    Rectangle checkbox = { co_list_container.x + co_list_container.width - 40, co_list_container.y + (j * 50) + base_font_size + 47, 20, 20 };

                    // put bigger invisible container for checkboxes to be easily clicked
                    Rectangle checkbox_container = { checkbox.x, checkbox.y - base_font_size / 1.5, 40, 40 };
                    DrawRectangleLinesEx(checkbox_container, 3, myTransparent);

                    // check if a checkbox is clicked
                    if (CheckCollisionPointRec(GetMousePosition(), checkbox_container) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
                        mouse_on_co_cb = i;
                        train_counter = 0;
                        train_list(i);
                        printf("%s", years[0]);
                    }

                    // if a checkbox was selected:
                    if (mouse_on_co_cb == i && mouse_on_train_cb == -1) {

                        DrawRectangleRec(checkbox, myRed);
                        DrawRectangleRec(window_container, mySkyBlue);

                        DrawTextEx(montserrat_regular, "Company name:", (Vector2) { input_co_name.x, input_co_name.y - 40 }, montserrat_regular.baseSize, 2, BLACK);

                        DrawRectangleRec(input_co_name, WHITE);

                        DrawTextEx(montserrat_regular, co_names[i], (Vector2) { input_co_name.x + 10, input_co_name.y + 8 }, montserrat_regular.baseSize, 2, BLACK);

                        DrawRectangleRec(btn_submit_co, myRed);
                        DrawRectangleLinesEx(btn_delete_co, 3, myRed);

                        DrawTextEx(montserrat_regular, "Submit", (Vector2) { btn_submit_co.x + btn_submit_co.width/2 - base_font_size - 16, btn_submit_co.y + btn_submit_co.height/2 - base_font_size/2 }, montserrat_regular.baseSize, 2, WHITE);
                        DrawTextEx(montserrat_regular, "Delete", (Vector2) { btn_delete_co.x + btn_delete_co.width / 2 - base_font_size - 14, btn_delete_co.y + btn_delete_co.height / 2 - base_font_size / 2 }, montserrat_regular.baseSize, 2, myRed);

                        // edit company name
                        {
                            int co_name_letterCount = strlen(co_names[i]);
                            // check if input box is clicked
                            if (CheckCollisionPointRec(GetMousePosition(), input_co_name) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_co_name = true;
                            else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_co_name = false;

                            // if input box was clicked:
                            if (isClicked_input_co_name)
                            {

                                DrawRectangleLinesEx(input_co_name, 3, myDarkBlue);

                                int key = GetKeyPressed();

                                input(key, &co_name_letterCount, co_names[i], 20);

                            }
                        }

                        // if submit btn was clicked:
                        if (CheckCollisionPointRec(GetMousePosition(), btn_submit_co) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
                            // edit db
                            co_modify(co_names[i], co_ids[i]);
                            con = 0;
                        }

                        // if delete btn was clicked:
                        if (CheckCollisionPointRec(GetMousePosition(), btn_delete_co) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
                            // delete from db
                            co_delete(co_ids[i]);
                            con = 0;
                        }

                    }
                    else if (mouse_on_co_cb == i && mouse_on_train_cb != -1) {
                        DrawRectangleRec(checkbox, myRed);
                    }
                    else {
                        DrawRectangleLinesEx(checkbox, 3, myRed);
                    }
                    j++;
                }

            }

            // check if add new company button is clicked
            if (CheckCollisionPointRec(GetMousePosition(), btn_new_co) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_btn_new_co = true;
            else if (!CheckCollisionPointRec(GetMousePosition(), window_container) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_btn_new_co = false;

            // if add company was clicked: 
            if (isClicked_btn_new_co) {

                mouse_on_co_cb = -1;

                DrawRectangleRec(window_container, mySkyBlue);

                DrawTextEx(montserrat_regular, "Company name:", (Vector2) { input_co_name.x, input_co_name.y - 40 }, montserrat_regular.baseSize, 2, BLACK);

                DrawRectangleRec(input_co_name, WHITE);

                if (CheckCollisionPointRec(GetMousePosition(), input_co_name) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_new_co = true;
                else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_new_co = false;

                if (isClicked_input_new_co) {

                    DrawRectangleLinesEx(input_co_name, 3, myDarkBlue);

                    int key = GetKeyPressed();

                    input(key, &co_name_holder_letterCount, co_name_holder, 100);

                }

                DrawTextEx(montserrat_regular, co_name_holder, (Vector2) { input_co_name.x + 10, input_co_name.y + 8 }, montserrat_regular.baseSize, 2, BLACK);

                DrawRectangleRec(btn_submit_co, myRed);
                DrawRectangleLinesEx(btn_delete_co, 3, myRed);

                DrawTextEx(montserrat_regular, "Add", (Vector2) { btn_submit_co.x + btn_submit_co.width/2 - base_font_size + 3, btn_submit_co.y + btn_submit_co.height/2 - base_font_size/2 }, montserrat_regular.baseSize, 2, WHITE);
                DrawTextEx(montserrat_regular, "Cancel", (Vector2) { btn_delete_co.x + btn_delete_co.width / 2 - base_font_size - 13, btn_delete_co.y + btn_delete_co.height / 2 - base_font_size / 2}, montserrat_regular.baseSize, 2, myRed);

                if (CheckCollisionPointRec(GetMousePosition(), btn_add_co) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
                    if (co_name_holder_letterCount > 0) {
                        co_add(co_name_holder);
                        con = 0;
                    }
                    else {
                        strcpy(input_errors, "Empty!");
                    }
                }

                DrawTextEx(montserrat_regular, input_errors, (Vector2) { btn_add_co.x, btn_delete_co.y + 60 }, montserrat_regular.baseSize, 2, myRed);

            }
        }

        // train list
        {
        // container
        DrawRectangleLinesEx(train_list_container, 3, myDarkBlue);

        // add train button
        DrawRectangleRec(btn_new_train, myRed);
        DrawTextEx(montserrat_regular, "add", (Vector2) { btn_new_train.x + btn_new_train.width / 2 - base_font_size + 2, btn_new_train.y + btn_new_train.height / 2 - base_font_size / 2 }, montserrat_regular.baseSize, 2, WHITE);

        // filter
        {
            DrawRectangleLinesEx(input_search_train_name, 3, myDarkBlue);
            DrawRectangleRec(btn_train_search, myTransparent);
            DrawTextEx(montserrat_small, "filter", (Vector2) { input_search_train_name.x + input_search_train_name.width/2 - base_font_size*1.2, input_search_train_name.y + 13 }, montserrat_small.baseSize, 2, GRAY);                DrawTextEx(montserrat_small, train_search_holder, (Vector2) { input_search_train_name.x + 60, input_search_train_name.y + 13 }, montserrat_small.baseSize, 2, myRed);

            if (CheckCollisionPointRec(GetMousePosition(), btn_train_search) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_search_train_name = true;
            else if (!CheckCollisionPointRec(GetMousePosition(), window_container) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_search_train_name = false;
            
        }

        int k = 0;
        for (int s = 0; s < train_counter; s++) {

            if (strcmp(show_train_names[s], " ") != 0) {
                // print all of names in the list
                DrawTextEx(montserrat_regular, show_train_names[s], (Vector2) { train_list_container.x + 10, train_list_container.y + (k * 50) + 70 }, montserrat_regular.baseSize, 2, BLACK);

                // put checkbox in front of names
                Rectangle train_checkbox = { train_list_container.x + train_list_container.width - 40, train_list_container.y + (k * 50) + base_font_size + 47, 20, 20 };

                // put bigger invisible container for checkboxes to be easily clicked
                Rectangle train_checkbox_container = { train_checkbox.x, train_checkbox.y - base_font_size / 1.5, 40, 40 };
                DrawRectangleLinesEx(train_checkbox_container, 3, myTransparent);

                // check if a checkbox is clicked
                if (CheckCollisionPointRec(GetMousePosition(), train_checkbox_container) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) mouse_on_train_cb = s;
                else if (CheckCollisionPointRec(GetMousePosition(), co_list_container) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) mouse_on_train_cb = -1;

                // if a checkbox was selected:
                if (mouse_on_train_cb == s) {

                    DrawRectangleRec(train_checkbox, myRed);
                    DrawRectangleRec(window_container, myYellow);

                    DrawTextEx(montserrat_regular, "Name", (Vector2) { window_container.x + window_container.width / 2 - base_font_size * 9, window_container.y + window_container.height / 2 - base_font_size * 14.5 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawRectangleRec(add_train_name, WHITE);
                    DrawTextEx(montserrat_regular, "From", (Vector2) { window_container.x + window_container.width / 2 - base_font_size * 9, window_container.y + window_container.height / 2 - base_font_size * 12 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawRectangleRec(add_train_origin, WHITE);
                    DrawTextEx(montserrat_regular, "To", (Vector2) { window_container.x + window_container.width / 2 - base_font_size * 9, window_container.y + window_container.height / 2 - base_font_size * 9.5 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawRectangleRec(add_train_destination, WHITE);
                    DrawTextEx(montserrat_regular, "Date", (Vector2) { window_container.x + window_container.width / 2 - base_font_size * 9, window_container.y + window_container.height / 2 - base_font_size * 7 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawRectangleRec(add_train_year, WHITE);
                    DrawRectangleRec(add_train_month, WHITE);
                    DrawRectangleRec(add_train_day, WHITE);
                    DrawTextEx(montserrat_regular, "Departure Time", (Vector2) { window_container.x + window_container.width / 2 - base_font_size * 9, window_container.y + window_container.height / 2 - base_font_size * 4.5 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawRectangleRec(add_train_hour, WHITE);
                    DrawRectangleRec(add_train_min, WHITE);
                    DrawTextEx(montserrat_big, "Personnel Info", (Vector2) { window_container.x + window_container.width / 2 - base_font_size * 6, window_container.y + window_container.height / 2 - base_font_size * 1 }, montserrat_big.baseSize, 2, BLACK);
                    DrawTextEx(montserrat_regular, "Trainmaster", (Vector2) { window_container.x + window_container.width / 2 - base_font_size * 9, window_container.y + window_container.height / 2 + base_font_size * 2 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawRectangleRec(add_train_master, WHITE);
                    DrawTextEx(montserrat_regular, "Driver", (Vector2) { window_container.x + window_container.width / 2 - base_font_size * 9, window_container.y + window_container.height / 2 + base_font_size * 4.5 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawRectangleRec(add_train_driver, WHITE);
                    DrawTextEx(montserrat_regular, "Adult Fee", (Vector2) { window_container.x + window_container.width / 2 - base_font_size * 14, window_container.y + window_container.height / 2 + base_font_size * 8 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawRectangleRec(add_train_afee, WHITE);
                    DrawTextEx(montserrat_regular, "Child Fee", (Vector2) { window_container.x + window_container.width / 2 - base_font_size * 14, window_container.y + window_container.height / 2 + base_font_size * 10.5 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawRectangleRec(add_train_cfee, WHITE);
                    DrawTextEx(montserrat_regular, "Compartments", (Vector2) { window_container.x + window_container.width / 2 + base_font_size * 1, window_container.y + window_container.height / 2 + base_font_size * 8 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawRectangleRec(add_train_comps, WHITE);
                    DrawTextEx(montserrat_regular, "Comp Capacity", (Vector2) { window_container.x + window_container.width / 2 + base_font_size * 1, window_container.y + window_container.height / 2 + base_font_size * 10.5 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawRectangleRec(add_train_capa, WHITE);

                    DrawTextEx(montserrat_regular, train_names[s], (Vector2) { add_train_name.x + 10, add_train_name.y + 13 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawTextEx(montserrat_regular, origins[s], (Vector2) { add_train_origin.x + 10, add_train_origin.y + 13 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawTextEx(montserrat_regular, destination[s], (Vector2) { add_train_destination.x + 10, add_train_destination.y + 13 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawTextEx(montserrat_regular, years[s], (Vector2) { add_train_year.x + 30, add_train_year.y + 10 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawTextEx(montserrat_regular, months[s], (Vector2) { add_train_month.x + 20, add_train_month.y + 10 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawTextEx(montserrat_regular, days[s], (Vector2) { add_train_day.x + 20, add_train_day.y + 10 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawTextEx(montserrat_regular, hours[s], (Vector2) { add_train_hour.x + 15, add_train_hour.y + 10 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawTextEx(montserrat_regular, mins[s], (Vector2) { add_train_min.x + 15, add_train_min.y + 10 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawTextEx(montserrat_regular, masters[s], (Vector2) { add_train_master.x + 15, add_train_master.y + 10 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawTextEx(montserrat_regular, drivers[s], (Vector2) { add_train_driver.x + 15, add_train_driver.y + 10 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawTextEx(montserrat_regular, drivers[s], (Vector2) { add_train_driver.x + 15, add_train_driver.y + 10 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawTextEx(montserrat_regular, afees[s], (Vector2) { add_train_afee.x + 15, add_train_afee.y + 10 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawTextEx(montserrat_regular, cfees[s], (Vector2) { add_train_cfee.x + 15, add_train_cfee.y + 10 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawTextEx(montserrat_regular, comps[s], (Vector2) { add_train_comps.x + 15, add_train_comps.y + 10 }, montserrat_regular.baseSize, 2, BLACK);
                    DrawTextEx(montserrat_regular, capapers[s], (Vector2) { add_train_capa.x + 15, add_train_capa.y + 10 }, montserrat_regular.baseSize, 2, BLACK);

                    DrawTexture(done, window_container.x + window_container.width/2 - base_font_size*2, window_container.y + window_container.height - base_font_size*2, WHITE);
                    DrawRectangleRec(done_container, myTransparent);

                    DrawTexture(delete, window_container.x + window_container.width / 2, window_container.y + window_container.height - base_font_size * 2, WHITE);
                    DrawRectangleRec(delete_container, myTransparent);

                    if (CheckCollisionPointRec(GetMousePosition(), add_train_name) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_name = true;
                    else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_name = false;

                    // if input box was clicked:
                    if (isClicked_input_train_name)
                    {

                        DrawRectangleLinesEx(add_train_name, 3, myDarkBlue);

                        int train_name_letterCount = strlen(train_names[s]);

                        int key = GetKeyPressed();

                        input(key, &train_name_letterCount, train_names[s], 100);

                    }

                    if (CheckCollisionPointRec(GetMousePosition(), add_train_origin) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_origin = true;
                    else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_origin = false;

                    // if input box was clicked:
                    if (isClicked_input_train_origin)
                    {

                        DrawRectangleLinesEx(add_train_origin, 3, myDarkBlue);

                        int train_origin_letterCount = strlen(origins[s]);

                        int key = GetKeyPressed();

                        input(key, &train_origin_letterCount, origins[s], 100);

                    }

                    if (CheckCollisionPointRec(GetMousePosition(), add_train_destination) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_destination = true;
                    else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_destination = false;

                    // if input box was clicked:
                    if (isClicked_input_train_destination)
                    {

                        DrawRectangleLinesEx(add_train_destination, 3, myDarkBlue);

                        int train_destination_letterCount = strlen(destination[s]);

                        int key = GetKeyPressed();

                        input(key, &train_destination_letterCount, destination[s], 100);

                    }

                    if (CheckCollisionPointRec(GetMousePosition(), add_train_year) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_year = true;
                    else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_year = false;

                    // if input box was clicked:
                    if (isClicked_input_train_year)
                    {

                        DrawRectangleLinesEx(add_train_year, 3, myDarkBlue);

                        int train_year_letterCount = strlen(years[s]);

                        int key = GetKeyPressed();

                        input(key, &train_year_letterCount, years[s], 4);

                    }

                    if (CheckCollisionPointRec(GetMousePosition(), add_train_month) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_month = true;
                    else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_month = false;

                    // if input box was clicked:
                    if (isClicked_input_train_month)
                    {

                        DrawRectangleLinesEx(add_train_month, 3, myDarkBlue);

                        int train_month_letterCount = strlen(months[s]);

                        int key = GetKeyPressed();

                        input(key, &train_month_letterCount, months[s], 2);

                    }

                    if (CheckCollisionPointRec(GetMousePosition(), add_train_day) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_day = true;
                    else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_day = false;

                    // if input box was clicked:
                    if (isClicked_input_train_day)
                    {

                        DrawRectangleLinesEx(add_train_day, 3, myDarkBlue);

                        int train_day_letterCount = strlen(days[s]);

                        int key = GetKeyPressed();

                        input(key, &train_day_letterCount, days[s], 2);

                    }

                    if (CheckCollisionPointRec(GetMousePosition(), add_train_hour) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_hour = true;
                    else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_hour = false;

                    // if input box was clicked:
                    if (isClicked_input_train_hour)
                    {

                        DrawRectangleLinesEx(add_train_hour, 3, myDarkBlue);

                        int train_hour_letterCount = strlen(hours[s]);

                        int key = GetKeyPressed();

                        input(key, &train_hour_letterCount, hours[s], 2);

                    }

                    if (CheckCollisionPointRec(GetMousePosition(), add_train_min) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_min = true;
                    else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_min = false;

                    // if input box was clicked:
                    if (isClicked_input_train_min)
                    {

                        DrawRectangleLinesEx(add_train_min, 3, myDarkBlue);

                        int train_min_letterCount = strlen(mins[s]);

                        int key = GetKeyPressed();

                        input(key, &train_min_letterCount, mins[s], 2);

                    }

                    if (CheckCollisionPointRec(GetMousePosition(), add_train_master) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_master = true;
                    else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_master = false;

                    // if input box was clicked:
                    if (isClicked_input_train_master)
                    {

                        DrawRectangleLinesEx(add_train_master, 3, myDarkBlue);

                        int train_master_letterCount = strlen(masters[s]);

                        int key = GetKeyPressed();

                        input(key, &train_master_letterCount, masters[s], 100);

                    }

                    if (CheckCollisionPointRec(GetMousePosition(), add_train_driver) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_driver = true;
                    else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_driver = false;

                    // if input box was clicked:
                    if (isClicked_input_train_driver)
                    {

                        DrawRectangleLinesEx(add_train_driver, 3, myDarkBlue);

                        int train_driver_letterCount = strlen(drivers[s]);

                        int key = GetKeyPressed();

                        input(key, &train_driver_letterCount, drivers[s], 100);

                    }

                    if (CheckCollisionPointRec(GetMousePosition(), add_train_afee) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_afee = true;
                    else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_afee = false;

                    // if input box was clicked:
                    if (isClicked_input_train_afee)
                    {

                        DrawRectangleLinesEx(add_train_afee, 3, myDarkBlue);

                        int train_afee_letterCount = strlen(afees[s]);

                        int key = GetKeyPressed();

                        input(key, &train_afee_letterCount, afees[s], 6);

                    }

                    if (CheckCollisionPointRec(GetMousePosition(), add_train_cfee) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_cfee = true;
                    else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_cfee = false;

                    // if input box was clicked:
                    if (isClicked_input_train_cfee)
                    {

                        DrawRectangleLinesEx(add_train_cfee, 3, myDarkBlue);

                        int train_cfee_letterCount = strlen(cfees[s]);

                        int key = GetKeyPressed();

                        input(key, &train_cfee_letterCount, cfees[s], 6);

                    }

                    if (CheckCollisionPointRec(GetMousePosition(), add_train_comps) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_comps = true;
                    else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_comps = false;

                    // if input box was clicked:
                    if (isClicked_input_train_comps)
                    {

                        DrawRectangleLinesEx(add_train_comps, 3, myDarkBlue);

                        int train_comps_letterCount = strlen(comps[s]);

                        int key = GetKeyPressed();

                        input(key, &train_comps_letterCount, comps[s], 4);

                    }

                    if (CheckCollisionPointRec(GetMousePosition(), add_train_capa) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_capa = true;
                    else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_capa = false;

                    // if input box was clicked:
                    if (isClicked_input_train_capa)
                    {
                        
                        DrawRectangleLinesEx(add_train_capa, 3, myDarkBlue);

                        int train_capa_letterCount = strlen(capapers[s]);

                        int key = GetKeyPressed();

                        input(key, &train_capa_letterCount, capapers[s], 1);

                    }

                    if (CheckCollisionPointRec(GetMousePosition(), done_container) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {

                        train_modify(train_ids[s], train_names[s], origins[s], destination[s], years[s], months[s], days[s], hours[s], mins[s], masters[s], drivers[s], afees[s], cfees[s], comps[s], capapers[s]);
                        con = 0;
                    }

                    if (CheckCollisionPointRec(GetMousePosition(), delete_container) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {

                        train_delete(train_ids[s]);
                        con = 0;
                    }
                    
                }
                else {
                    DrawRectangleLinesEx(train_checkbox, 3, myRed);
                }
                k++;
            }

        }
        // check if add new company button is clicked
        if (CheckCollisionPointRec(GetMousePosition(), btn_new_train) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_btn_new_train = true;
        else if (!CheckCollisionPointRec(GetMousePosition(), window_container) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_btn_new_train = false;

        if (isClicked_btn_new_train || isClicked_input_search_train_name) {

            if (isClicked_input_search_train_name) filter = 1;

            if (mouse_on_co_cb != -1) temp = mouse_on_co_cb;
            mouse_on_train_cb = -1;
            mouse_on_co_cb = -1;

            DrawRectangleRec(window_container, myYellow);

            DrawTextEx(montserrat_regular, "Name", (Vector2) { window_container.x + window_container.width / 2 - base_font_size * 9, window_container.y + window_container.height / 2 - base_font_size * 14.5 }, montserrat_regular.baseSize, 2, BLACK);
            DrawRectangleRec(add_train_name, WHITE);
            DrawTextEx(montserrat_regular, "From", (Vector2) { window_container.x + window_container.width / 2 - base_font_size * 9, window_container.y + window_container.height / 2 - base_font_size * 12 }, montserrat_regular.baseSize, 2, BLACK);
            DrawRectangleRec(add_train_origin, WHITE);
            DrawTextEx(montserrat_regular, "To", (Vector2) { window_container.x + window_container.width / 2 - base_font_size * 9, window_container.y + window_container.height / 2 - base_font_size * 9.5 }, montserrat_regular.baseSize, 2, BLACK);
            DrawRectangleRec(add_train_destination, WHITE);
            DrawTextEx(montserrat_regular, "Date", (Vector2) { window_container.x + window_container.width / 2 - base_font_size * 9, window_container.y + window_container.height / 2 - base_font_size * 7 }, montserrat_regular.baseSize, 2, BLACK);
            DrawRectangleRec(add_train_year, WHITE);
            DrawRectangleRec(add_train_month, WHITE);
            DrawRectangleRec(add_train_day, WHITE);
            DrawTextEx(montserrat_regular, "Departure Time", (Vector2) { window_container.x + window_container.width / 2 - base_font_size * 9, window_container.y + window_container.height / 2 - base_font_size * 4.5 }, montserrat_regular.baseSize, 2, BLACK);
            DrawRectangleRec(add_train_hour, WHITE);
            DrawRectangleRec(add_train_min, WHITE);
            DrawTextEx(montserrat_big, "Personnel Info", (Vector2) { window_container.x + window_container.width / 2 - base_font_size * 6, window_container.y + window_container.height / 2 - base_font_size * 1 }, montserrat_big.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, "Trainmaster", (Vector2) { window_container.x + window_container.width / 2 - base_font_size * 9, window_container.y + window_container.height / 2 + base_font_size * 2 }, montserrat_regular.baseSize, 2, BLACK);
            DrawRectangleRec(add_train_master, WHITE);
            DrawTextEx(montserrat_regular, "Driver", (Vector2) { window_container.x + window_container.width / 2 - base_font_size * 9, window_container.y + window_container.height / 2 + base_font_size * 4.5 }, montserrat_regular.baseSize, 2, BLACK);
            DrawRectangleRec(add_train_driver, WHITE);
            DrawTextEx(montserrat_regular, "Adult Fee", (Vector2) { window_container.x + window_container.width / 2 - base_font_size * 14, window_container.y + window_container.height / 2 + base_font_size * 8 }, montserrat_regular.baseSize, 2, BLACK);
            DrawRectangleRec(add_train_afee, WHITE);
            DrawTextEx(montserrat_regular, "Child Fee", (Vector2) { window_container.x + window_container.width / 2 - base_font_size * 14, window_container.y + window_container.height / 2 + base_font_size * 10.5 }, montserrat_regular.baseSize, 2, BLACK);
            DrawRectangleRec(add_train_cfee, WHITE);
            DrawTextEx(montserrat_regular, "Compartments", (Vector2) { window_container.x + window_container.width / 2 + base_font_size * 1, window_container.y + window_container.height / 2 + base_font_size * 8 }, montserrat_regular.baseSize, 2, BLACK);
            DrawRectangleRec(add_train_comps, WHITE);
            DrawTextEx(montserrat_regular, "Comp Capacity", (Vector2) { window_container.x + window_container.width / 2 + base_font_size * 1, window_container.y + window_container.height / 2 + base_font_size * 10.5 }, montserrat_regular.baseSize, 2, BLACK);
            DrawRectangleRec(add_train_capa, WHITE);

            DrawTextEx(montserrat_regular, train_name_holder, (Vector2) { add_train_name.x + 10, add_train_name.y + 8 }, montserrat_regular.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, train_origin_holder, (Vector2) { add_train_origin.x + 10, add_train_origin.y + 8 }, montserrat_regular.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, train_dest_holder, (Vector2) { add_train_destination.x + 10, add_train_destination.y + 8 }, montserrat_regular.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, train_year_holder, (Vector2) { add_train_year.x + 10, add_train_year.y + 8 }, montserrat_regular.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, train_month_holder, (Vector2) { add_train_month.x + 10, add_train_month.y + 8 }, montserrat_regular.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, train_day_holder, (Vector2) { add_train_day.x + 10, add_train_day.y + 8 }, montserrat_regular.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, train_hour_holder, (Vector2) { add_train_hour.x + 10, add_train_hour.y + 8 }, montserrat_regular.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, train_min_holder, (Vector2) { add_train_min.x + 10, add_train_min.y + 8 }, montserrat_regular.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, train_master_holder, (Vector2) { add_train_master.x + 10, add_train_master.y + 8 }, montserrat_regular.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, train_driver_holder, (Vector2) { add_train_driver.x + 10, add_train_driver.y + 8 }, montserrat_regular.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, train_afee_holder, (Vector2) { add_train_afee.x + 10, add_train_afee.y + 8 }, montserrat_regular.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, train_cfee_holder, (Vector2) { add_train_cfee.x + 10, add_train_cfee.y + 8 }, montserrat_regular.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, train_comps_holder, (Vector2) { add_train_comps.x + 10, add_train_comps.y + 8 }, montserrat_regular.baseSize, 2, BLACK);
            DrawTextEx(montserrat_regular, train_capa_holder, (Vector2) { add_train_capa.x + 10, add_train_capa.y + 8 }, montserrat_regular.baseSize, 2, BLACK);

            if (CheckCollisionPointRec(GetMousePosition(), add_train_name) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_name_holder = true;
            else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_name_holder = false;

            // if input box was clicked:
            if (isClicked_input_train_name_holder)
            {

                DrawRectangleLinesEx(add_train_name, 3, myDarkBlue);

                int key = GetKeyPressed();

                input(key, &train_name_holder_counter, train_name_holder, 100);

            }

            if (CheckCollisionPointRec(GetMousePosition(), add_train_origin) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_origin_holder = true;
            else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_origin_holder = false;

            // if input box was clicked:
            if (isClicked_input_train_origin_holder)
            {

                DrawRectangleLinesEx(add_train_origin, 3, myDarkBlue);

                int key = GetKeyPressed();

                input(key, &train_origin_holder_counter, train_origin_holder, 100);

            }

            if (CheckCollisionPointRec(GetMousePosition(), add_train_destination) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_destination_holder = true;
            else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_destination_holder = false;

            // if input box was clicked:
            if (isClicked_input_train_destination_holder)
            {

                DrawRectangleLinesEx(add_train_destination, 3, myDarkBlue);

                int key = GetKeyPressed();

                input(key, &train_dest_holder_counter, train_dest_holder, 100);

            }

            if (CheckCollisionPointRec(GetMousePosition(), add_train_year) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_year_holder = true;
            else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_year_holder = false;

            // if input box was clicked:
            if (isClicked_input_train_year_holder)
            {

                DrawRectangleLinesEx(add_train_year, 3, myDarkBlue);

                int key = GetKeyPressed();

                input(key, &train_year_holder_counter, train_year_holder, 4);

            }

            if (CheckCollisionPointRec(GetMousePosition(), add_train_month) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_month_holder = true;
            else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_month_holder = false;

            // if input box was clicked:
            if (isClicked_input_train_month_holder)
            {

                DrawRectangleLinesEx(add_train_month, 3, myDarkBlue);

                int key = GetKeyPressed();

                input(key, &train_month_holder_counter, train_month_holder, 2);

            }

            if (CheckCollisionPointRec(GetMousePosition(), add_train_day) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_day_holder = true;
            else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_day_holder = false;

            // if input box was clicked:
            if (isClicked_input_train_day_holder)
            {

                DrawRectangleLinesEx(add_train_day, 3, myDarkBlue);

                int key = GetKeyPressed();

                input(key, &train_day_holder_counter, train_day_holder, 2);

            }

            if (CheckCollisionPointRec(GetMousePosition(), add_train_hour) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_hour_holder = true;
            else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_hour_holder = false;

            // if input box was clicked:
            if (isClicked_input_train_hour_holder)
            {

                DrawRectangleLinesEx(add_train_hour, 3, myDarkBlue);

                int key = GetKeyPressed();

                input(key, &train_hour_holder_counter, train_hour_holder, 2);

            }

            if (CheckCollisionPointRec(GetMousePosition(), add_train_min) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_min_holder = true;
            else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_min_holder = false;

            // if input box was clicked:
            if (isClicked_input_train_min_holder)
            {

                DrawRectangleLinesEx(add_train_min, 3, myDarkBlue);

                int key = GetKeyPressed();

                input(key, &train_min_holder_counter, train_min_holder, 2);

            }

            if (CheckCollisionPointRec(GetMousePosition(), add_train_master) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_master_holder = true;
            else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_master_holder = false;

            // if input box was clicked:
            if (isClicked_input_train_master_holder)
            {

                DrawRectangleLinesEx(add_train_master, 3, myDarkBlue);

                int key = GetKeyPressed();

                input(key, &train_master_holder_counter, train_master_holder, 100);

            }

            if (CheckCollisionPointRec(GetMousePosition(), add_train_driver) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_driver_holder = true;
            else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_driver_holder = false;

            if (isClicked_input_train_afee_holder)
            {

                DrawRectangleLinesEx(add_train_afee, 3, myDarkBlue);

                int key = GetKeyPressed();

                input(key, &train_afee_holder_counter, train_afee_holder, 6);

            }

            if (CheckCollisionPointRec(GetMousePosition(), add_train_afee) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_afee_holder = true;
            else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_afee_holder = false;

            if (isClicked_input_train_cfee_holder)
            {

                DrawRectangleLinesEx(add_train_cfee, 3, myDarkBlue);

                int key = GetKeyPressed();

                input(key, &train_cfee_holder_counter, train_cfee_holder, 6);

            }

            if (CheckCollisionPointRec(GetMousePosition(), add_train_cfee) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_cfee_holder = true;
            else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_cfee_holder = false;

            if (isClicked_input_train_comps_holder)
            {

                DrawRectangleLinesEx(add_train_comps, 3, myDarkBlue);

                int key = GetKeyPressed();

                input(key, &train_comps_holder_counter, train_comps_holder, 4);

            }

            if (CheckCollisionPointRec(GetMousePosition(), add_train_comps) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_comps_holder = true;
            else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_comps_holder = false;

            if (isClicked_input_train_capa_holder)
            {

                DrawRectangleLinesEx(add_train_capa, 3, myDarkBlue);

                int key = GetKeyPressed();

                input(key, &train_capa_holder_counter, train_capa_holder, 1);

            }

            if (CheckCollisionPointRec(GetMousePosition(), add_train_capa) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_capa_holder = true;
            else if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) isClicked_input_train_capa_holder = false;

            // if input box was clicked:
            if (isClicked_input_train_driver_holder)
            {

                DrawRectangleLinesEx(add_train_driver, 3, myDarkBlue);

                int key = GetKeyPressed();

                input(key, &train_driver_holder_counter, train_driver_holder, 100);

            }

            DrawTexture(done, window_container.x + window_container.width / 2 - base_font_size * 2, window_container.y + window_container.height - base_font_size * 2, WHITE);
            DrawRectangleRec(done_container, myTransparent);

            DrawTexture(delete, window_container.x + window_container.width / 2, window_container.y + window_container.height - base_font_size * 2, WHITE);
            DrawRectangleRec(delete_container, myTransparent);
            
            if (filter == -1) {

                if (CheckCollisionPointRec(GetMousePosition(), done_container) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {

                    train_add(co_names[temp], train_name_holder, train_origin_holder, train_dest_holder, train_year_holder, train_month_holder, train_day_holder, train_hour_holder, train_min_holder, train_master_holder, train_driver_holder, train_afee_holder, train_cfee_holder, train_comps_holder, train_capa_holder);
                    con = 0;
                }

                if (CheckCollisionPointRec(GetMousePosition(), delete_container) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {

                    isClicked_btn_new_train = false;
                    
                }

            }
            else {

                if (CheckCollisionPointRec(GetMousePosition(), done_container) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {

                    search_train(train_name_holder, train_origin_holder, train_dest_holder, train_year_holder, train_month_holder, train_day_holder, train_hour_holder, train_min_holder, train_master_holder, train_driver_holder, train_afee_holder, train_cfee_holder, train_comps_holder, train_capa_holder);

                }

                if (CheckCollisionPointRec(GetMousePosition(), delete_container) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
                    
                    for (int i = 0; i < train_counter; i++) {
                        strcpy(show_train_names[i], train_names[i]);
                        train_name_cutter();
                    }

                }

            }

        }
        }

        // db error
        DrawTextEx(montserrat_regular, initial_db_error, (Vector2) { window_container.x + 100, window_container.y + 100 }, montserrat_regular.baseSize, 2, myRed);

        EndDrawing();

    }

    UnloadFont(montserrat_regular);
    UnloadTexture(delete);
    UnloadTexture(done);
    UnloadTexture(co_searcht);

    CloseWindow();

    // if con = 0 --> refresh window
    if (con == 0) {
        co_counter = 0;
        run_admin_panel(screenWidth, screenHeight);
    }
}