#include <time.h> 
#include "raylib.h"

#define myDarkBlue CLITERAL{36, 48, 94, 255}
#define myLightBlue CLITERAL{55, 71, 133, 255}
#define mySkyBlue CLITERAL{168, 208, 230, 255}
#define myRed CLITERAL{247, 108, 108, 255}
#define myYellow CLITERAL{248, 233, 161, 255}

/********************************************************************

    Splash Screen

********************************************************************/
void run_splash(const int screenWidth, const int screenHeight) {

    /* Responsive Image Size */
    int imageWidth = screenWidth / 2.5;
    int imageheight = imageWidth * 2 / 3;

    /* Initializing Window */
    InitWindow(screenWidth, screenHeight, "Train Reservation");

    /* Loading Image */
    Image Splash_Train = LoadImage("Resources/mockup/images/splash_train.png");
    ImageResize(&Splash_Train, imageWidth, imageheight);
    Texture2D texture = LoadTextureFromImage(Splash_Train);
    UnloadImage(Splash_Train);

    /* UI Desgin */
    {
        BeginDrawing();

        ClearBackground(myDarkBlue);

        /* Displaying Image */
        DrawTexture(texture, screenWidth / 2 - texture.width / 2, screenHeight / 2 - texture.height / 2, WHITE);

        EndDrawing();

        UnloadTexture(texture);
    }

    /* Splash Screen Time */
    int timeCounter = 0;
    clock_t prevTime = clock();

    while (timeCounter < 3000)
    {
        clock_t currentTime = clock() - prevTime;
        timeCounter = currentTime * 1000 / CLOCKS_PER_SEC;
    }

    CloseWindow();
}