#include <stdio.h>
#include <string.h>
#include "sqlite3.h"
#include "check_login.h"

#pragma warning(disable: 4996)

sqlite3* db;

char prename[100];

int t_modify_callback(void* NotUsed, int argc, char** argv, char** azColName) {
    strcpy(prename, argv[0]);
    printf("%s", prename);
    return 0;
}

int co_modify_callback(void* NotUsed, int argc, char** argv, char** azColName) {
    return 0;
}

int co_delete_callback(void* NotUsed, int argc, char** argv, char** azColName) {
    return 0;
}

int co_add_callback(void* NotUsed, int argc, char** argv, char** azColName) {
    return 0;
}

void co_modify(char name[100], int id) {

	if (!connect()) {
		return -1;
	}

    int trc;
    char tsql[1000];
    sprintf(tsql, "SELECT name FROM companies WHERE id is %d", id);
    char* tzErrMsg = 0;

    trc = sqlite3_exec(db, tsql, t_modify_callback, 0, &tzErrMsg);

    if (trc != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", tzErrMsg);
        sqlite3_free(tzErrMsg);
        return -1;
    }

    int urc;
    char usql[1000];
    sprintf(usql, "UPDATE trains set company = \'%s\' WHERE company = \'%s\'", name, prename);
    char* uzErrMsg = 0;

    urc = sqlite3_exec(db, usql, co_modify_callback, 0, &uzErrMsg);

    if (urc != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", uzErrMsg);
        sqlite3_free(uzErrMsg);
        return -1;
    }

    int rc;
    char sql[1000];
    sprintf(sql, "UPDATE companies set name = \'%s\' WHERE id is %d", name, id);
    char* zErrMsg = 0;

    rc = sqlite3_exec(db, sql, co_modify_callback, 0, &zErrMsg);

    if (rc != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        return -1;
    }

}

void co_delete(int id) {

    if (!connect()) {
        return -1;
    }

    int rc;
    char sql[1000];
    sprintf(sql, "DELETE FROM companies WHERE id = \'%d\'", id);
    char* zErrMsg = 0;

    rc = sqlite3_exec(db, sql, co_delete_callback, 0, &zErrMsg);

    if (rc != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        return -1;
    }

}

void co_add(char name[100]) {

    if (!connect()) {
        return -1;
    }

    int rc;
    char sql[1000];
    sprintf(sql, "INSERT INTO companies (name) VALUES (\'%s\')", name);
    char* zErrMsg = 0;

    rc = sqlite3_exec(db, sql, co_add_callback, 0, &zErrMsg);

    if (rc != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        return -1;
    }

}